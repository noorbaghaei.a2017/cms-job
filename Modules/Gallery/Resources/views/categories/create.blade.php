@include('core::layout.modules.category.create',[

    'title'=>__('core::categories.create'),
    'parent'=>'gallery',
    'model'=>'gallery',
    'directory'=>'galleries',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'gallery.category.store'],

])








