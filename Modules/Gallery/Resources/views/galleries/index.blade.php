@include('core::layout.modules.index',[

    'title'=>__('gallery::galleries.index'),
    'items'=>$items,
    'parent'=>'gallery',
    'model'=>'gallery',
    'directory'=>'galleries',
    'collect'=>__('gallery::galleries.collect'),
    'singular'=>__('gallery::galleries.singular'),
    'create_route'=>['name'=>'galleries.create'],
    'edit_route'=>['name'=>'galleries.edit','name_param'=>'gallery'],
    'destroy_route'=>['name'=>'galleries.destroy','name_param'=>'gallery'],
     'search_route'=>true,
     'setting_route'=>true,
      'category_route'=>true,
     'pagination'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])

