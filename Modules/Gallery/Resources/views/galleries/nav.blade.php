<li>
        <a href="{{route('galleries.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('gallery.icons.gallery')}}"> </i>
                              </span>
            <span class="nav-text">{{__('gallery::galleries.collect')}}</span>
        </a>
    </li>

