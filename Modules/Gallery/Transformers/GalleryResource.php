<?php

namespace Modules\Gallery\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class GalleryResource extends Resource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'thumbnail'=>(!$this->Hasmedia('images'))  ? asset('img/no-img.gif') : $this->getFirstMediaUrl('images', 'medium') ,
            'title'=>$this->title,
            'token'=>$this->token,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
        ];
    }
}
