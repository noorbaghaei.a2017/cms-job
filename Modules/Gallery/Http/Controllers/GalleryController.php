<?php

namespace Modules\Gallery\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Setting;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Http\Requests\GalleryRequest;
use Modules\Gallery\Transformers\GalleryCollection;
use Modules\Question\Http\Requests\QuestionRequest;

class GalleryController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;

    //category

    protected $route_categories_index='gallery::categories.index';
    protected $route_categories_create='gallery::categories.create';
    protected $route_categories_edit='gallery::categories.edit';
    protected $route_categories='gallery.categories';


//question

    protected $route_questions_index='gallery::questions.index';
    protected $route_questions_create='gallery::questions.create';
    protected $route_questions_edit='gallery::questions.edit';
    protected $route_questions='galleries.index';


//notification

    protected $notification_store='gallery::galleries.store';
    protected $notification_update='gallery::galleries.update';
    protected $notification_delete='gallery::galleries.delete';
    protected $notification_error='gallery::galleries.error';



    public function __construct()
    {
        $this->entity=new Gallery();
        $this->class= Gallery::class;
        $this->setting=new Setting();

        $this->middleware('permission:gallery-list');
        $this->middleware('permission:gallery-create')->only(['create','store']);
        $this->middleware('permission:gallery-edit' )->only(['edit','update']);
        $this->middleware('permission:gallery-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->where('galleryable_type',Setting::class)->paginate(config('cms.paginate'));

            $galleries = new GalleryCollection($items);

            $data= collect($galleries->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('gallery::galleries.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('gallery::galleries.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $parent_galleries=$this->entity->latest()->whereParent(0)->get();
            $categories=Category::latest()->where('model',Gallery::class)->get();
            return view('gallery::galleries.create',compact('parent_galleries','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param QuestionRequest $request
     * @return void
     */
    public function store(GalleryRequest $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $setting=Setting::first();
            $saved=$setting->galleries()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                'text'=>$request->input('text'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'status'=>$request->input('status'),
                'order'=>$request->input('order'),
                'token'=>tokenGenerate()
            ]);

            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $saved->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }else{
                return redirect(route("galleries.index"))->with('message',__('gallery::galleries.store'));
            }

        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $categories=Category::latest()->where('model',Gallery::class)->get();
            $parent_galleries=$this->entity->latest()->whereParent(0)->get();
            return view('gallery::galleries.edit',compact('item','parent_galleries','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(GalleryRequest $request, $token)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "status"=>$request->input('status'),
                'text'=>$request->input('text'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "order"=>orderInfo($request->input('order'))
            ]);

            $this->entity->syncTags($request->input('tags'));

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$update){
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }else{
                return redirect(route("galleries.index"))->with('message',__('gallery::galleries.update'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }else{
                return redirect(route("galleries.index"))->with('message',__('gallery::galleries.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
