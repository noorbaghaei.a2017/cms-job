<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/events', 'EventController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'events'], function () {
        Route::get('/gallery/{event}', 'EventController@gallery')->name('event.gallery');
        Route::post('/gallery/store/{event}', 'EventController@galleryStore')->name('event.gallery.store');
        Route::get('/gallery/destroy/{media}', 'EventController@galleryDestroy')->name('event.gallery.destroy');
    });


    Route::group(["prefix"=>'search'], function () {
        Route::post('/events', 'EventController@search')->name('search.event');
    });


    Route::group(["prefix"=>'event/categories'], function () {
        Route::get('/', 'EventController@categories')->name('event.categories');
        Route::get('/create', 'EventController@categoryCreate')->name('event.category.create');
        Route::post('/store', 'EventController@categoryStore')->name('event.category.store');
        Route::get('/edit/{category}', 'EventController@categoryEdit')->name('event.category.edit');
        Route::patch('/update/{category}', 'EventController@categoryUpdate')->name('event.category.update');

    });

    Route::group(["prefix"=>'event/questions'], function () {
        Route::get('/{event}', 'EventController@question')->name('event.questions');
        Route::get('/create/{event}', 'EventController@questionCreate')->name('event.question.create');
        Route::post('/store/{event}', 'EventController@questionStore')->name('event.question.store');
        Route::delete('/destroy/{question}', 'EventController@questionDestroy')->name('event.question.destroy');
        Route::get('/edit/{event}/{question}', 'EventController@questionEdit')->name('event.question.edit');
        Route::patch('/update/{question}', 'EventController@questionUpdate')->name('event.question.update');
    });

});
