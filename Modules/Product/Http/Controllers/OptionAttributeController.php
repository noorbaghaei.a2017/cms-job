<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Http\Controllers\HasOption;
use Modules\Product\Entities\OptionAttribute;
use Modules\Product\Entities\Repository\OptionAttribute\OptionAttributeRepositoryInterface;
use Modules\Product\Transformers\OptionAttribute\OptionAttributeCollection;

class OptionAttributeController extends Controller
{

    use HasOption;

    protected $entity;
    protected $class;
    private $repository;

//option

    protected $route_options_index='product::attributes.options.index';
    protected $route_options_create='product::attributes.options.create';
    protected $route_options_edit='product::attributes.options.edit';
    protected $route_options='attributes.options';

//notification

    protected $notification_store='product::attributes.store';
    protected $notification_update='product::attributes.update';
    protected $notification_delete='product::attributes.delete';
    protected $notification_error='product::attributes.error';


    public function __construct(OptionAttributeRepositoryInterface $repository)
    {
        $this->entity=new OptionAttribute();

        $this->class=OptionAttribute::class;

        $this->repository=$repository;

        $this->middleware('permission:product-list')->only('index');
        $this->middleware('permission:product-create')->only(['create','store']);
        $this->middleware('permission:product-edit' )->only(['edit','update']);
        $this->middleware('permission:product-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new OptionAttributeCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {

            if(
                !isset($request->title) &&
                !isset($request->code)
            ){
                $items=$this->repository->getAll();

                $result = new OptionAttributeCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }

            $items=$this->entity
                ->where("title",trim($request->title))
                ->orwhere("code",trim($request->code))
                ->paginate(config('cms.paginate'));

            $result = new OptionAttributeCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('product::attributes.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->icon=$request->input('icon');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();


            if(!$saved){
                return redirect()->back()->with('error',__('product::attributes.error'));
            }else{
                return redirect(route("attributes.index"))->with('message',__('product::attributes.store'));
            }



        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('product::attributes.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "icon"=>$request->input('icon'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order'))
            ]);

            $this->entity->replicate();

            if(!$updated){
                return redirect()->back()->with('error',__('product::attributes.error'));
            }else{
                return redirect(route("attributes.index"))->with('message',__('product::attributes.update'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
