<?php

namespace Modules\Product\Transformers\Product;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'thumbnail'=>(!$this->Hasmedia('images'))  ? asset('img/no-img.gif') : $this->getFirstMediaUrl('images') ,
            'title'=>$this->title ,
            'code'=>$this->code ,
            'price'=>$this->PriceFormat ,
            'status'=>$this->ShowStatus ,
            'special'=>$this->ShowSpecial ,
            'category'=>$this->NameCategory ,
            'view'=>$this->view ,
            'like'=>$this->like ,
            'questions'=>$this->question ,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
            'token'=>$this->token,

        ];
    }
}

