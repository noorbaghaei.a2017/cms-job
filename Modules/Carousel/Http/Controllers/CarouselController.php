<?php

namespace Modules\Carousel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Mockery\Exception;
use Modules\Carousel\Entities\Carousel;
use Modules\Carousel\Entities\Repository\CarouselRepositoryInterface;
use Modules\Carousel\Http\Requests\CarouselRequest;
use Modules\Carousel\Transformers\CarouselCollection;
use Modules\Information\Entities\Information;

class CarouselController extends Controller
{
    protected $entity;

    private $repository;

    public function __construct(CarouselRepositoryInterface $repository)
    {
        $this->entity=new Carousel();
        $this->repository=$repository;

        $this->middleware('permission:carousel-list')->only('index');
        $this->middleware('permission:carousel-create')->only(['create','store']);
        $this->middleware('permission:carousel-edit' )->only(['edit','update']);
        $this->middleware('permission:carousel-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new CarouselCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('carousel::carousels.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->slug) &&
                !isset($request->order)
            ){
                $items=$this->repository->getAll();
                $result = new CarouselCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
             $items=$this->entity
                ->where("title",trim($request->title))
                ->where("slug",trim($request->slug))
                ->where("order",trim($request->order))
                ->paginate(config('cms.paginate'));
            $result = new CarouselCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param CarouselRequest $request
     * @return Response
     */
    public function store(CarouselRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$saved){
                return redirect()->back()->with('error',__('carousel::carousels.error'));
            }else{
                return redirect(route("carousels.index"))->with('message',__('carousel::carousels.store'));
            }

        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->firstOrFail();
            return view('carousel::carousels.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(CarouselRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order')),
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('carousel::carousels.error'));
            }else{
                return redirect(route("carousels.index"))->with('message',__('carousel::carousels.update'));
            }
        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('carousel::carousels.error'));
            }else{
                return redirect(route("carousels.index"))->with('message',__('carousel::carousels.delete'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
