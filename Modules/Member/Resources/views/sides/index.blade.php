@include('core::layout.modules.index',[

    'title'=>__('member::sides.index'),
    'items'=>$items,
    'parent'=>'member',
    'model'=>'side',
    'directory'=>'sides',
    'collect'=>__('member::sides.collect'),
    'singular'=>__('member::sides.singular'),
    'create_route'=>['name'=>'sides.create'],
    'edit_route'=>['name'=>'sides.edit','name_param'=>'side'],
    'destroy_route'=>['name_param'=>'side'],
    'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'TransTitle',
    __('cms.create_date')=>'AgoTime'
    ],
        'detail_data'=>[
    __('cms.title')=>'TransTitle',
    __('cms.create_date')=>'created_at'
    ],
])





