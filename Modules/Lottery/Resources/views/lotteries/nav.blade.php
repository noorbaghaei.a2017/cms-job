
    <li>
        <a href="{{route('lotteries.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('lottery.icons.lottery')}}"></i>
                              </span>

            <span class="nav-text">{{__('lottery::lotteries.collect')}}</span>
        </a>
    </li>

