<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Advertising\Entities\Position;
use Modules\Advertising\Entities\Repository\PositionRepositoryInterface;
use Modules\Advertising\Transformers\PositionCollection;

class PositionController extends Controller
{
    protected $entity;
    private $repository;

    public function __construct(PositionRepositoryInterface $repository)
    {
        $this->entity=new Position();
        $this->repository=$repository;

        $this->middleware('permission:advertising-list')->only('index');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new PositionCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->order) &&
                !isset($request->symbol)
            ){

                $items=$this->repository->getAll();

                $result = new PositionCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("order",'LIKE','%'.trim($request->order).'%')
                ->where("symbol",'LIKE','%'.trim($request->symbol).'%')
                ->paginate(config('cms.paginate'));
            $result = new PositionCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $items=Position::latest()->get();
            $parents=$this->entity->latest()->whereParent(0)->get();
            return view('advertising::positions.create',compact('items','parents'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Position::whereToken($request->input('parent'))->first();
            }

            $saved=Position::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('advertising::positions.error'));
            }else{
                return redirect(route('positions.index'))->with('message',__('advertising::positions.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {

            $item=Position::whereToken($token)->first();
            $parents=Position::latest()->whereParent(0)->where('token','!=',$token)->get();

            return view('advertising::positions.edit',compact('item','parents'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Position::whereToken($request->input('parent'))->first();
            }

            $item=Position::whereToken($token)->first();
            $updated=$item->update([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('advertising.positions.error'));
            }else{
                return redirect(route('positions.index'))->with('message',__('advertising::positions.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
