<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Advertising\Entities\Repository\TypeAdvertisingRepositoryInterface;
use Modules\Advertising\Entities\TypeAdvertising;
use Modules\Advertising\Transformers\TypeAdvertisingCollection;

class TypeController extends Controller
{
    protected $entity;
    private $repository;

    public function __construct(TypeAdvertisingRepositoryInterface $repository)
    {
        $this->entity=new TypeAdvertising();
        $this->repository=$repository;

        $this->middleware('permission:advertising-list')->only('index');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();
            $result = new TypeAdvertisingCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->order) &&
                !isset($request->symbol)
            ){

                $items=$this->repository->getAll();

                $result = new TypeAdvertisingCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("order",'LIKE','%'.trim($request->order).'%')
                ->where("symbol",'LIKE','%'.trim($request->symbol).'%')
                ->paginate(config('cms.paginate'));
            $result = new TypeAdvertisingCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $items=TypeAdvertising::latest()->get();
            return view('advertising::types.create',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $saved=TypeAdvertising::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('advertising::types.error'));
            }else{
                return redirect(route('types.index'))->with('message',__('advertising::types.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=TypeAdvertising::whereToken($token)->first();
            return view('advertising::types.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $item=TypeAdvertising::whereToken($token)->first();
            $updated=$item->update([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'status'=>$request->input('status'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('advertising.types.error'));
            }else{
                return redirect(route('types.index'))->with('message',__('advertising::types.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
