<?php


namespace Modules\Sms\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class IdepardazanFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'idepardazan';
    }
}
