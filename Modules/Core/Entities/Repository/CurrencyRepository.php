<?php


namespace Modules\Core\Entities\Repository;


use Modules\Core\Entities\Currency;

class CurrencyRepository implements CurrencyRepositoryInterface
{

    public function getAll()
    {
      return Currency::latest()->get();
    }
}
