<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;

class UserCompany extends Model
{
    protected $table="user_companies";

    protected $fillable = ['name','phone','national_id','postal_code','address','mobile','count_member','country','city'];

    public function companyable(){
        return $this->morphTo();
    }

    public function client_info(){
        return $this->belongsTo(Client::class,'client','id');
    }

}
