<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $fillable = [
        'website',
        'youtube',
        'github',
        'gitlab',
        'facebook',
        'twitter',
        'telegram',
        'instagram',
        'whatsapp',
        'pintrest',
        'address',
        'private',
        'about'
    ];

    public function infoable(){
       return $this->morphTo();
    }

}
