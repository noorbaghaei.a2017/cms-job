<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['state_id'];

    protected $table ="cities";
}
