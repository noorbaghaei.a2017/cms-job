<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['amount'];

    public function priceable()
    {
        return $this->morphTo();
    }
}
