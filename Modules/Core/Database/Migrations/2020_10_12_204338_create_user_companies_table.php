<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('companyable');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal_code',10)->nullable();
            $table->string('national_id')->nullable();
            $table->text('address')->nullable();
            $table->string('mobile', 11)->nullable();
            $table->string('country')->nullable();
            $table->string('count_member')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_companies');
    }
}
