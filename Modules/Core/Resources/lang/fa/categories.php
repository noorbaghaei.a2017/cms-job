<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید دسته بندی جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید دسته بندی خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست دسته بندی ها",
    "singular"=>"دسته بندی",
    "collect"=>"دسته بندی ها",
    "permission"=>[
        "category-full-access"=>"دسترسی کامل به دسته بندی ها",
        "category-list"=>"لیست دسته بندی ها",
        "category-delete"=>"ویرایش دسته بندی",
        "category-create"=>"ایجاد دسته بندس",
        "category-edit"=>"ویرایش دسته بندی",
    ]
];
