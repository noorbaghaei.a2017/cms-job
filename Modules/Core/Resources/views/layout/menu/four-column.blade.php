
<div class="col-md-12 sub-menu_content">
    @if(!is_null(hasTitleMegaMenu($menu->id,1)))
    <span class="sub-menu-title"> <h4> {{hasTitleMegaMenu($menu->id,1)->symbol}} </h4> </span>
    @endif
    <ul class="nav flex-column">
        @foreach(childColumn($menu->id,1) as $child)
            <li class="sub-item">
                <a class="sub-link" href="{{$child->href}}"><i class="fas fa-caret-right"></i>
                    {{$child->symbol}}</a>
            </li>
        @endforeach
    </ul>
</div>
<!-- /.col-md-4  -->
<div class="col-md-4 sub-menu_content d-flex justify-content-center">
    <div class="col-md-10">
        @if(!is_null(hasTitleMegaMenu($menu->id,2)))
        <span class="sub-menu-title"> <h4> {{hasTitleMegaMenu($menu->id,2)->symbol}} </h4> </span>
        @endif
        <ul class="nav flex-column">
            @foreach(childColumn($menu->id,2) as $child)
                <li class="sub-item">
                    <a class="sub-link" href="{{$child->href}}"><i
                            class="fas fa-caret-right"></i> {{$child->symbol}}</a>
                </li>
            @endforeach

        </ul>
    </div>
</div>
<!-- /.col-md-4  -->
<div class="col-md-4 sub-menu_content d-flex justify-content-center">
    <div class="col-md-10">
        @if(!is_null(hasTitleMegaMenu($menu->id,3)))
        <span class="sub-menu-title"> <h4> {{hasTitleMegaMenu($menu->id,3)->symbol}} </h4> </span>
        @endif
        <ul class="nav flex-column">
            @foreach(childColumn($menu->id,3) as $child)
                <li class="sub-item">
                    <a class="sub-link" href="{{$child->href}}"><i
                            class="fas fa-caret-right"></i>{{$child->symbol}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- /.col-md-4  -->
<div class="col-md-3 sub-menu_content d-flex justify-content-center">
    <div class="col-md-10">
        @if(!is_null(hasTitleMegaMenu($menu->id,4)))
        <span class="sub-menu-title"> <h4> {{hasTitleMegaMenu($menu->id,4)->symbol}} </h4> </span>
        @endif
        <ul class="nav flex-column">
            @foreach(childColumn($menu->id,3) as $child)
            <li class="sub-item">
                <a class="sub-link" href="{{$child->href}}"><i
                        class="fas fa-caret-right"></i>{{$item->symbol}}</a>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- /.col-md-4  -->
