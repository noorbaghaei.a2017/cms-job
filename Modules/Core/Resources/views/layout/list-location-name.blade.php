@if(is_null($item))

    <div id="msg">

    </div>
    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="co" class="form-control-label">{{__('cms.countries')}} </label>
                    <select dir="rtl" class="form-control"  id="co" name="country" required >

                    </select>
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="state" class="form-control-label">{{__('cms.states')}} </label>
                    <select dir="rtl" class="form-control"  id="state" name="state" required >

                    </select>
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="city" class="form-control-label">{{__('cms.cities')}} </label>
                    <select dir="rtl" class="form-control"  id="city" name="city" required >

                    </select>
                </div>
            </div>


        </div>
    </div>
@else
    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="co" class="form-control-label">{{__('cms.countries')}} </label>
                    <select dir="rtl" class="form-control"  id="co" name="country" required >

                        @foreach($countries as $key=>$value)

                            <option value="{{$value->id}}" {{$item->country==$value->name ? "selected" : ""}}>{{__('countries.'.$value->name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="state" class="form-control-label">{{__('cms.states')}} </label>
                    <select dir="rtl" class="form-control"  id="state" name="state" required >
                        @foreach($states as $key=>$value)

                            <option value="{{$value->id}}" {{$item->states==$value->name ? "selected" : ""}}>{{__('states.'.$value->name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="city" class="form-control-label">{{__('cms.cities')}} </label>
                    <select dir="rtl" class="form-control"  id="city" name="city" required >
                        @foreach($cities as $key=>$value)

                            <option value="{{$value->id}}" {{$item->city==$value->name ? "selected" : ""}}>{{__('cities.'.$value->name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


        </div>
    </div>


@endif

