<li>
        <a href="{{route('questions.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('questions.icons.question')}}"> </i>
                              </span>
            <span class="nav-text">{{__('questions::questions.collect')}}</span>
        </a>
    </li>

