@include('core::layout.modules.index',[

    'title'=>__('brand::brands.index'),
    'items'=>$items,
    'parent'=>'brand',
    'model'=>'brand',
    'directory'=>'brands',
    'collect'=>__('brand::brands.collect'),
    'singular'=>__('brand::brands.singular'),
    'create_route'=>['name'=>'brands.create'],
    'edit_route'=>['name'=>'brands.edit','name_param'=>'brand'],
    'destroy_route'=>['name'=>'brands.destroy','name_param'=>'brand'],
     'search_route'=>true,
     'setting_route'=>true,
     'pagination'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
    __('cms.href')=>'href',
       __('cms.update_date')=>'AgoTimeUpdate',
     __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
        __('cms.slug')=>'slug',
        __('cms.href')=>'href',
       __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',

    ],


])
