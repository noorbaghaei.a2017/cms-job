<?php


namespace Modules\Customer\Entities\Repository;


use Modules\Customer\Entities\Customer;

class CustomerRepository implements CustomerRepositoryInterface
{

    public function getAll()
    {
        return Customer::latest()->get();
    }
}
