@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'portfolio',
    'model'=>'portfolio',
    'directory'=>'portfolios',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'portfolio.category.update','param'=>'category'],

])








