
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.search')}} </h2>

            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                @include('core::layout.alert-danger')
                <form role="form" method="post" action="{{route('search.user')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">

                        <div class="col-sm-3">
                            <label for="email" class="form-control-label">{{__('cms.email')}}  </label>
                            <input type="email" value="{{isset($request->email) ? $request->email : ""}}" name="email" class="form-control" id="last_name" >
                        </div>
                        <div class="col-sm-3">
                            <label for="mobile" class="form-control-label">{{__('cms.mobile')}}  </label>
                            <input type="text" value="{{isset($request->mobile) ?  $request->mobile :""}}" name="mobile" class="form-control" id="mobile" >
                        </div>
                        <div class="col-sm-3">
                            <label for="mobile" class="form-control-label">{{__('cms.mobile')}}  </label>
                            <select dir="rtl" class="form-control" id="roles" name="roles" required>

                                @foreach($roles as $role)
                                    <option value="{{$role}}">{{$role}}</option>
                                @endforeach


                            </select>
                        </div>

                    </div>

                    <div class="form-group row m-t-md">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

