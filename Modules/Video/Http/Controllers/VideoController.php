<?php

namespace Modules\Video\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Setting;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Video\Entities\Video;
use Modules\Video\Http\Requests\VideoRequest;
use Modules\Video\Transformers\VideoCollection;

class VideoController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;

    //category

    protected $route_categories_index='video::categories.index';
    protected $route_categories_create='video::categories.create';
    protected $route_categories_edit='video::categories.edit';
    protected $route_categories='video.categories';


//question

    protected $route_questions_index='video::questions.index';
    protected $route_questions_create='video::questions.create';
    protected $route_questions_edit='video::questions.edit';
    protected $route_questions='videos.index';


//notification

    protected $notification_store='video::videos.store';
    protected $notification_update='video::videos.update';
    protected $notification_delete='video::videos.delete';
    protected $notification_error='video::videos.error';

    public function __construct()
    {
        $this->entity=new Video();
        $this->class=Video::class;
        $this->setting=new Setting();

        $this->middleware('permission:video-list');
        $this->middleware('permission:video-create')->only(['create','store']);
        $this->middleware('permission:video-edit' )->only(['edit','update']);
        $this->middleware('permission:video-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->where('videoable_type',Setting::class)->paginate(config('cms.paginate'));

            $videos = new VideoCollection($items);

            $data= collect($videos->response()->getData())->toArray();

            return view('core::response.index',compact('data'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('video::videos.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('video::videos.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $parent_videos=$this->entity->latest()->whereParent(0)->get();
            $categories=Category::latest()->where('model',Video::class)->get();
            return view('video::videos.create',compact('parent_videos','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param QuestionRequest $request
     * @return void
     */
    public function store(VideoRequest $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $setting=Setting::first();
            $saved=$setting->videos()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'href'=>$request->input('href'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                'text'=>$request->input('text'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'status'=>$request->input('status'),
                'order'=>$request->input('order'),
                'token'=>tokenGenerate()
            ]);

            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $saved->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('video')){
                $saved->addMedia($request->file('video'))->toMediaCollection(config('cms.collection-video'));
            }


            if(!$saved){
                return redirect()->back()->with('error',__('video::videos.error'));
            }else{
                return redirect(route("videos.index"))->with('message',__('video::videos.store'));
            }

        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $parent_videos=$this->entity->latest()->whereParent(0)->get();
            $categories=Category::latest()->where('model',Video::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('video::videos.edit',compact('item','parent_videos','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(VideoRequest $request, $token)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "status"=>$request->input('status'),
                'text'=>$request->input('text'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "href"=>$request->input('href'),
                "order"=>orderInfo($request->input('order'))
            ]);

            $this->entity->syncTags($request->input('tags'));

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('video')){
                destroyMedia($this->entity,config('cms.collection-video'));
                $this->entity->addMedia($request->file('video'))->toMediaCollection(config('cms.collection-video'));
            }


            if(!$update){
                return redirect()->back()->with('error',__('video::videos.error'));
            }else{
                return redirect(route("videos.index"))->with('message',__('video::videos.update'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            if($this->entity->Hasmedia(config('cms.collection-video'))){
                destroyMedia($this->entity,config('cms.collection-video'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('video::videos.error'));
            }else{
                return redirect(route("videos.index"))->with('message',__('video::videos.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
