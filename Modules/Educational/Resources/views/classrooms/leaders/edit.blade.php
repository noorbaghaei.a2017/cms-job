@include('core::layout.modules.leader.edit',[

    'title'=>__('core::leaders.create'),
    'item'=>$item,
    'parent'=>'educational',
    'model'=>'classroom',
    'directory'=>'classrooms',
    'collect'=>__('core::leaders.collect'),
    'singular'=>__('core::leaders.singular'),
   'update_route'=>['name'=>'classroom.leader.update','param'=>'leader'],

])








