@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'educational',
    'model'=>'classroom',
    'directory'=>'classrooms',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'classroom.category.update','param'=>'category'],

])








