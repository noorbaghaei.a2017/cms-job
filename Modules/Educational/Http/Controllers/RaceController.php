<?php

namespace Modules\Educational\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Modules\Client\Entities\Client;
use Modules\Client\Http\Requests\ForceStudentRegister;
use Modules\Core\Entities\Award;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\Leader;
use Modules\Core\Entities\User;
use Modules\Core\Entities\UserAction;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasLeader;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Educational\Entities\Race;
use Modules\Educational\Entities\Repository\RaceRepositoryInterface;
use Modules\Educational\Http\Requests\RaceRequest;
use Modules\Educational\Http\Requests\RegisterScoreRequest;
use Modules\Educational\Transformers\RaceCollection;
use Modules\Event\Http\Requests\EventRequest;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Order\Entities\Order;
use Modules\Payment\Entities\Payment;

class RaceController extends Controller
{

    use HasQuestion,HasCategory,HasGallery,HasLeader;

    protected $entity;
    protected $class;
    private $repository;

    //leader

    protected $route_leaders_index='educational::races.leaders.index';
    protected $route_leaders_create='educational::races.leaders.create';
    protected $route_leaders_edit='educational::races.leaders.edit';
    protected $route_leaders='race.leaders';



//category

    protected $route_categories_index='educational::races.categories.index';
    protected $route_categories_create='educational::races.categories.create';
    protected $route_categories_edit='educational::races.categories.edit';
    protected $route_categories='race.categories';


//question

    protected $route_questions_index='educational::races.questions.index';
    protected $route_questions_create='educational::races.questions..race.create';
    protected $route_questions_edit='educational::races.questions.edit';
    protected $route_questions='races.index';



//gallery

    protected $route_gallery_index='educational::races.gallery';
    protected $route_gallery='races.index';

//notification

    protected $notification_store='educational::races.store';
    protected $notification_update='educational::races.update';
    protected $notification_delete='educational::races.delete';
    protected $notification_error='educational::races.error';


    public function __construct(RaceRepositoryInterface $repository)
    {
        $this->entity=new Race();
        $this->class=Race::class;
        $this->repository=$repository;

        $this->middleware('permission:race-list')->only('index');
        $this->middleware('permission:race-create')->only(['create','store']);
        $this->middleware('permission:race-edit' )->only(['edit','update']);
        $this->middleware('permission:race-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new RaceCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->slug) &&
                !isset($request->price)
            ){
                $items=$this->repository->getAll();

                $result = new RaceCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->where("price",'LIKE','%'.trim($request->price).'%')
                ->paginate(config('cms.paginate'));

            $result = new RaceCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $leaders=Leader::latest()->where('model',$this->class)->get();
            $prerequisites=Leader::latest()->where('model',$this->class)->get();
            $currencies=Currency::latest()->get();
            $members=Member::with('professor')->has('professor')->get();
            $categories=Category::latest()->where('model',Race::class)->get();
            $parent_races=$this->entity->latest()->whereParent(0)->get();
            return view('educational::races.create',compact('parent_races','categories','members','currencies','leaders','prerequisites'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(RaceRequest $request)
    {
        try {
            DB::beginTransaction();
            $parent=-1;
            if($request->input('parent')!=-1){
                $parent=$this->entity->whereToken($request->input('parent'))->firstOrFail();
            }
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->leader=Leader::whereToken($request->input('leader'))->first()->id;
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->icon=$request->input('icon');
            $this->entity->query=$request->input('query');
            $this->entity->status=$request->input('status');
            $this->entity->text=$request->input('text');
            $this->entity->start_at=convertJalali($request->date_start,$request->time_start);
            $this->entity->end_at=convertJalali($request->date_end,$request->time_end);
            $this->entity->sign_start_at=convertJalali($request->sign_date_start,$request->sign_time_start);
            $this->entity->sign_end_at=convertJalali($request->sign_date_end,$request->sign_time_end);
            $this->entity->currency=Currency::whereToken($request->input('currency'))->first()->id;
            $this->entity->event_place=$request->input('event_place');
            $this->entity->total_hour=$request->input('total_hour');
            $this->entity->professor=Member::whereToken($request->input('professor'))->first()->id;
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->prerequisites=json_encode($request->input('prerequisites'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title-discount'),
                'amount'=>$request->input('amount-discount'),
                'code'=>$request->input('code-discount'),
                'percentage'=>$request->input('percentage-discount'),
                'start_at'=>now()
            ]);


            $this->entity->week()->create([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);

            $this->entity->price()->create([
                'amount'=>$request->input('price'),
            ]);



            $this->entity->analyzer()->create();


            $this->entity->syncTags($request->input('tags'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('educational::races.error'));
            }else{
                DB::commit();
                return redirect(route("races.index"))->with('message',__('educational::races.store'));
            }


        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $leaders=Leader::latest()->where('model',$this->class)->get();
            $prerequisites=Leader::latest()->where('model',$this->class)->get();
            $currencies=Currency::latest()->get();
            $categories=Category::latest()->where('model',Race::class)->get();
            $members=Member::with('professor')->has('professor')->get();
            $parent_races=$this->entity->latest()->whereParent(0)->where('token','!=',$token)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('educational::races.edit',compact('item','categories','parent_races','currencies','members','leaders','prerequisites'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public function studentForce(Request $request,$race)
    {
        try {
            $item=Race::findOrFail($race);
            $userAction=UserAction::where('actionable_id',$race)->where('actionable_type',Race::class)->where('status',1)->pluck('client')->toArray();
            $clients=Client::whereNotIn('id',$userAction)->get();
            return view('educational::races.students.create',compact('clients','race','item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function studentRegisterForce(ForceStudentRegister $request,$race)
    {
        try {
            $model=Race::with('price')->find($race);

            $order=Order::create([
                'order_id'=>123+now()->timestamp,
                'status'=>1,
                'total_price'=>$model->price->amount
            ]);


            $model->orderList()->create([
                'order'=>$order->id,
                'count'=>1,
                'price'=>$model->price->amount,
                'discount'=>0,
            ]);

            $payment=Payment::create([
                'client'=>Client::where('token',$request->student)->first()->id,
                'order'=>$order->id,
                'status'=>1,
                'title'=>'پرداخت حضوری',
                'token'=>tokenGenerate(),
            ]);

            $model->userAction()->create([
                'client'=>Client::where('token',$request->student)->first()->id,
                'status'=>1,
                'title'=>'ثبت نام',
                'payment'=>$payment->id,
                'start_at'=>now(),
            ]);
            if(!$payment){
                return redirect()->back()->with('error',__('educational::students.error'));
            }else{
                return redirect(route("student.show.race",['race'=>$model->token]))->with('message',__('educational::students.store'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function showScore(Request $request,$data,$token)
    {
        try {
            $item=Client::whereToken($token)->firstOrFail();
            $awards=Award::latest()->get();
            $data=$this->entity->whereToken($data)->firstOrFail();
            return view('educational::races.students.score',compact('item','data','awards'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function studentDestroyForce(Request $request,$client,$race)
    {
        try {
            $item=Race::whereToken($race)->firstOrFail();
            $client=Client::whereToken($client)->firstOrFail();
            $useraction=UserAction::where('actionable_type',Race::class)->where('actionable_id',$item->id)->where('client',$client->id)->firstOrFail();
            $payment=Payment::findOrFail($useraction->payment);
            $order=Order::findOrFail($payment->order);
            $delete_order=$order->delete();
            $delete_useraction=$useraction->delete();
            if(!$delete_order && !$delete_useraction){
                return redirect()->back()->with('error',__('educational::students.error'));
            }else{
                return redirect(route("student.show.race",['race'=>$item->token]))->with('message',__('educational::students.store'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public function registerScore(RegisterScoreRequest $request,$data,$token)
    {
        try {
            $item=Client::whereToken($token)->firstOrFail();
            $data=$this->entity->whereToken($data)->firstOrFail();
            $action=UserAction::where('client',$item->id)->where('actionable_id',$data->id)->where('actionable_type',Race::class)->firstOrFail();

            $result=$action->update([
                'award'=>$request->input('award'),
                'rate'=>$request->input('rate'),
                'record'=>$request->input('record'),
                'score'=>$request->input('score')
            ]);

            if(!$result){
                return redirect()->back()->with('error',__('educational::races.error'));
            }else{
                return redirect(route("student.show.race",['race'=>$data->token]))->with('message',__('educational::races.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function registerScoreMulti(Request $request,$data,$token)
    {
        try {
            $item=Client::whereToken($token)->firstOrFail();
            $data=$this->entity->whereToken($data)->firstOrFail();
            $childs=getChild($data,'race');

            foreach ($childs as $child){
                $action=UserAction::where('client',$item->id)->where('actionable_id',$child->id)->where('actionable_type',Race::class)->firstOrFail();
                $result=$action->update([
                    'award'=>$request->input($child->token.'_award'),
                    'rate'=>$request->input($child->token.'_rate'),
                    'record'=>$request->input($child->token.'_record'),
                    'score'=>$request->input($child->token.'_score')
                ]);
            }

            if(!$result){
                return redirect()->back()->with('error',__('educational::races.error'));
            }else{
                return redirect(route("student.show.race",['race'=>$data->token]))->with('message',__('educational::races.store'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param RaceRequest $request
     * @param $token
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            DB::beginTransaction();
            $validator=Validator::make($request->all(),[
                'title'=>'required',
                'sign_time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
                'time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
                'date_start'=>'required',
                'time_end'=>'required',
                'date_end'=>'required',
                'professor'=>'required',
                'text'=>'required',
                'excerpt'=>'required',
                'event_place'=>'required',
                'currency'=>'required',
                'total_hour'=>'required|numeric|min:1',
                'leader'=>'required',
                'order'=>'required|numeric|min:1',
                'price'=>'numeric|min:0'
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }
            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "icon"=>$request->input('icon'),
                "order"=>$request->input('order'),
                "start_at"=>convertJalali($request->input('date_start'),$request->input('time_start')),
                "end_at"=>convertJalali($request->input('date_end'),$request->input('time_end')),
                "sign_start_at"=>convertJalali($request->input('sign_date_start'),$request->input('sign_time_start')),
                "sign_end_at"=>convertJalali($request->input('sign_date_end'),$request->input('sign_time_end')),
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "status"=>$request->input('status'),
                "query"=>$request->input('query'),
                "capacity"=>$request->input('capacity'),
                "event_place"=>$request->input('event_place'),
                "total_hour"=>$request->input('total_hour'),
                "professor"=>Member::whereToken($request->input('professor'))->firstOrFail()->id,
                "currency"=>Currency::whereToken($request->input('currency'))->first()->id,
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "leader"=>Leader::whereToken($request->input('leader'))->first()->id,
                "text"=>$request->input('text'),
                "prerequisites"=>json_encode($request->input('prerequisites')),
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $this->entity->price()->update([
                'amount'=>$request->input('price'),
            ]);

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->discount()->update([
                'title'=>$request->input('title-discount'),
                'amount'=>$request->input('amount-discount'),
                'code'=>$request->input('code-discount'),
                'percentage'=>$request->input('percentage-discount'),
                'start_at'=>now()
            ]);

            $this->entity->week()->update([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);


            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('educational::races.error'));
            }else{
                DB::commit();
                return redirect(route("races.index"))->with('message',__('educational::races.update'));
            }


        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function studentShow(Request $request,$token){

        try {
            DB::beginTransaction();
            $data=Race::whereToken($token)->firstOrFail();
            $clients=UserAction::where('actionable_type',Race::class)->where('actionable_id',$data->id)->pluck('client')->toArray();
            $items=Client::whereIn('id',$clients)->get();
            DB::commit();
            return  view('educational::races.students.index',compact('items','data'));
        } catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }

    }
}
