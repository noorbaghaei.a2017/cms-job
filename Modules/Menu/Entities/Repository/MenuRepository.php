<?php


namespace Modules\Menu\Entities\Repository;


use Modules\Menu\Entities\Menu;

class MenuRepository implements MenuRepositoryInterface
{
    public function getAll()
    {
       return Menu::latest()->get();
    }
}
