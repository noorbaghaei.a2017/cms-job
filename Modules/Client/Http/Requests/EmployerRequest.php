<?php

namespace Modules\Client\Http\Requests;

use App\Rules\Nationalcode;
use Illuminate\Foundation\Http\FormRequest;

class EmployerRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        if ($this->has('mobile'))
            $this->merge(['mobile'=>checkZeroFirst($this->mobile)]);


    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',
            'password'=>'min:8',
            'email'=>'required|unique:clients,email,'.$this->token.',token',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:clients,mobile,'.$this->token.',token',
            'identity_card' => ['required',new Nationalcode()]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
