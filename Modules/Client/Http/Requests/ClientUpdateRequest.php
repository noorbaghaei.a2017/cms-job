<?php

namespace Modules\Client\Http\Requests;

use App\Rules\Nationalcode;
use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'unique:clients,email,'.$this->client.',id',
            'username'=>'unique:clients,username,'.$this->client.',id',
            'address'=>'required',
            'postal_code'=>'nullable|numeric|max:10',
            'identity_card' => ['required',new Nationalcode()]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
