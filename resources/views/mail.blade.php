<h1>Hallo Guten Tag!</h1>
<p>Liebe Dr. Moheb</p>
<p>Dieses Formular wurde von Ihrer Website eingereicht und enthält die folgenden Informationen:</p>

<p><strong>Geschlecht:</strong> {{$gender}}</p>
<p><strong>Name:</strong> {{$name}}</p>
<p><strong>Email:</strong> {{$email}}</p>
<p><strong>Phone:</strong> {{$phone}}</p>
<p><strong>WELCHER BEREICH STÖRT SIE?</strong> {{$ihr}}</p>
<p><strong>WELCHE HAARFARBE / HABEN SIE?</strong> {{$blond}}</p>
<p><strong>SEIT WANN LEIDEN SIE UNTER HAARAUSFALL?</strong> {{$Jahr}}</p>
<p><strong>HATTEN SIE BEREITS EINE HAARTRANSPLANTATION?</strong> {{$eine}}</p>
<p><strong>WIE SCHLIMM EMPFINDEN SIE IHRE AKTUELLE HAARSITUATION?</strong> {{$schlimm}}</p>
<p><strong>WANN SOLL DIE BEHANDLUNG STATTFINDEN?</strong> {{$behandlung}}</p>

<p>Vielen Dank -</p>
