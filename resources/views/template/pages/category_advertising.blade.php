@extends('template.app')

@section('content')


    <!-- job category wrapper start-->
    <div class="jb_category_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                    <div class="jb_heading_wraper">

                        <h3> دسته بندی آگهی ها</h3>
                    </div>
                </div>
                @foreach($items as $item)

                    <div class="col-lg-3 col-md-6 col-sm-12" style="margin-bottom: 15px">
                        <form method="GET" action="{{route('filter.advert')}}">
                            @csrf
                            <div class="jb_browse_category jb_cover">
                                <input hidden class="pixel-radio" name="category[]" checked value="{{$item->id}}" type="checkbox" >
                                <button type="submit" style="cursor: pointer;border: none;background: transparent">
                                    <div class="hover-block"></div>

                                    <i class="flaticon-code"></i>

                                    <h6>{{$item->symbol}}</h6>

                                    <p>{{count($item->advertisings)}}</p>
                                </button>
                            </div>
                        </form>
                    </div>


                @endforeach

            </div>
        </div>
    </div>
    <!-- job category wrapper end-->


@endsection
