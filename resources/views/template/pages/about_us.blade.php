@extends('template.app')

@section('content')

    <!-- work Wrapper Start -->
    <div class="iner_abt_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="about_slider_wrapper float_left">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="about_image">
                                    <img src="{{asset('template/images/about1.jpg')}}" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="{{asset('template/images/about2.jpg')}}" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="{{asset('template/images/about3.jpg')}}" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="{{asset('template/images/about2.jpg')}}" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-12 col-12 col-sm-12 offset-lg-1">
                    <div class="about_text_wrapper">
<p>
    تصاویری از شرکت کار نیک
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- work Wrapper end -->
    @include('template.sections.members')
    <div class="jb_cover">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="job_newsletter_wrapper jb_cover">
                        <div class="jb_newslwtteter_left">
                            <h2> به دنبال یک شغل</h2>
                            <p>لیست همه مشاغل ها </p>
                        </div>
                        <div class="jb_newslwtteter_button">
                            <div class="header_btn search_btn news_btn jb_cover">

                                <a href="{{route('advertisings')}}">جستجو</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
