
@extends('template.app')

@section('content')

    <!-- contact_icon_section start-->
    <div class="contact_icon_section jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                    <div class="jb_heading_wraper">

                        <h3>تماس با ما</h3>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="contact_main jb_cover">
                        <div class="contact_rotate">
                            <i class="fas fa-phone"></i>
                        </div>

                      <p>
{{json_decode($setting->phone,true)[0]}}
                      </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="contact_main jb_cover">

                        <div class="contact_rotate">
                            <i class="fas fa-envelope"></i>
                        </div>

                        <p><a href="mailto::{{$setting->email}}">{{$setting->email}} </a>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="contact_main jb_cover">

                        <div class="contact_rotate">

                            <i class="fas fa-map-marker-alt"></i>
                        </div>

                        <p>
                        {{$setting->address}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact info section end -->
    <!-- map wrapper  start-->
    <div class="map_wrapper_top jb_cover">
        <div class="map_wrapper map2_wrapper">
            <div>

                {!! $setting->google_map !!}
            </div>
        </div>
        <div class="contact_field_wrapper comments_form">
            @include('core::layout.alert-danger')
            @include('core::layout.alert-success')
            <div class="jb_heading_wraper left_rivew_heading">
                <h3>پیشنهادات شما</h3>
            </div>
            <form name="contact_form" action="{{route('send.idea')}}" method="get">
                @csrf
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-pos">
                            <div class="form-group i-name">

                                <input type="text" class="form-control require" name="name"  id="namTen-first" placeholder=" نام" >
                                <i class="fas fa-user-alt"></i>
                            </div>
                        </div>
                    </div>

                    <!-- /.col-md-12 -->
                    <div class="col-lg-12 col-md-12">
                        <div class="form-e">
                            <div class="form-group i-email">
                                <label class="sr-only">ایمیل </label>
                                <input type="email" class="form-control require" name="email"  id="emailTen" placeholder=" ایمیل " data-valid="email" data-error="ایمیل باید معتبر باشد" >
                                <i class="fas fa-envelope"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="form-e">
                            <div class="form-group i-email">
                                <label class="sr-only">موضوع </label>
                                <input type="text" class="form-control require" name="subject"  id="subject" placeholder=" موضوع "   >
                                <i class="fas fa-info"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-m">
                            <div class="form-group i-message">

                                <textarea class="form-control require" name="message" rows="5" id="messageTen" placeholder=" پیام" ></textarea>
                                <i class="fas fa-comment"></i>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-12 -->
                    <div class="col-md-12">
                        <div class="tb_es_btn_div">
                            <div class="response"></div>
                            <div class="tb_es_btn_wrapper">
                                <button type="submit">ارسال</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- map wrapper  end-->
    <!-- news app wrapper start-->
    <div class="news_letter_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="job_newsletter_wrapper jb_cover">
                        <div class="jb_newslwtteter_left">
                            <h2> به دنبال یک شغل</h2>
                            <p>لیست همه مشاغل ها </p>
                        </div>
                        <div class="jb_newslwtteter_button">
                            <div class="header_btn search_btn news_btn jb_cover">

                                <a href="{{route('advertisings')}}">جستجو</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- news app wrapper end-->



@endsection
