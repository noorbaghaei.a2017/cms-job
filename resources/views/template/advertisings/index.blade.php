@extends('template.app')

@section('content')

    <!-- navi wrapper End -->
    <!--job listing filter  wrapper start-->
    <div class="job_filter_listing_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-12 d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <form action="{{route('filter.advert')}}" method="GET">
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#order" role="button">
                            <h6>مرتب سازی </h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{(isset($query) && $order=='new') || (isset($query) && $order=='view') ? "in show " :"" }}" id="order">

                                <p class="job_field">
                                    <label for="f1">
                                    <input type="radio" {{isset($query) && $order=='new' ? "checked" : ""}} value="new" id="f1" name="order">

                                        جدیدترین
                                        </label>
                                </p>
                            <p class="job_field">
                                <label for="f2">
                                <input type="radio" {{isset($query) && $order=='view' ? "checked" : ""}} value="view" id="f2" name="order">
                                    پربازدید ترین
                                </label>
                            </p>


                        </div>
                    </div>

                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#category" role="button">
                            <h6>دسته بندی </h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_category) || $array_category==null ? "" :"in show" }}" id="category">
                            @foreach($all_category_advertisings as $category)
                            <p>
                                <input type="checkbox" {{isset($query) && in_array($category->id,$array_category) ? "checked " :"" }} value="{{$category->id}}" id="c{{$category->id}}" name="category[]">
                                <label for="c{{$category->id}}" >{{$category->symbol}}</label>
                            </p>
                            @endforeach

                        </div>

                    </div>
                        <div class="job_filter_category_sidebar jb_cover">
                            <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#company" role="button">
                                <h6>شرکت  </h6>
                            </div>

                            <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_company) || $array_company==null ? "" :"in show" }}" id="company">
                                @foreach($companies as $company)
                                    <p>
                                        <input type="checkbox" {{isset($query) && in_array($company->id,$array_company) ? "checked " :"" }} value="{{$company->id}}" id="com{{$company->id}}" name="company[]">
                                        <label for="com{{$company->id}}" >{{$company->company->name}}</label>
                                    </p>
                                @endforeach

                            </div>

                        </div>

                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#skill" role="button">
                            <h6>مهارت </h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_skill) || $array_skill==null ? "" :"in show" }}" id="skill">
                            @foreach($skills as $skill)
                            <p >
                                <input type="checkbox" {{isset($query) && in_array($skill->title,$array_skill) ? "checked " :"" }} value="{{$skill->title}}" id="s{{$skill->id}}" name="skill[]">
                                <label for="s{{$skill->id}}">{{$skill->symbol}}</label>
                            </p>
                            @endforeach


                        </div>
                    </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#salary" role="button">
                            <h6>حقوق</h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($item_salary) || $item_salary==null ? "" :"in show" }}" id="salary">
                            @foreach($salaries as $salary)
                                <p >
                                    <input type="checkbox" {{isset($query) && $salary->title==$item_salary ? "checked" : ""}} value="{{$salary->title}}" id="sa{{$salary->id}}" name="salary">
                                    <label for="sa{{$salary->id}}">{{$salary->symbol}}</label>
                                </p>
                            @endforeach


                        </div>
                    </div>

                    <div class="job_filter_category_sidebar jb_cover">
                        <input type="submit" class="btn btn-block btn-pink" style="color: #fdfdfd"  value="اعمال فیلتر">
                    </div>

                    </form>


                    <div class="jp_add_resume_wrapper jb_cover">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">
                            <a class="ad"  target="_blank" href="{{getAd('BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO')->href}}" data-param="BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO">
                            @if(getAd('BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO')->status==1 && expireTime(getAd('BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO')->start_at,getAd('BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO')->end_at))

                                <img src="{{getAd('BjdyHzj2b9iTtIrOWIY5WtrVyrBAsBwj6eO')->getFirstMediaUrl('images')}}" alt="{{$setting->name}}"  width="80" />
                            @else
                                <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" width="60"  />
                            @endif
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="job_listing_left_side jb_cover">
                        <div class="filter-area jb_cover">
                            <div class="list-grid">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#grid"> <i class="flaticon-four-grid-layout-design-interface-symbol"></i></a>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#list"><i class="flaticon-list"></i></a>
                                    </li>

                                </ul>
                            </div>

                        </div>
                        <div class="tab-content btc_shop_index_content_tabs_main jb_cover">
                            <div id="grid" class="tab-pane active">
                                <div class="row">
                                    @if(!isset($query))
                                    @foreach($items as $item)

                                    <div class="col-lg-6 col-md-6 col-sm-12">

                                        <div class="job_listing_left_fullwidth job_listing_grid_wrapper jb_cover">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="jp_job_post_side_img">
                                                        <a href="{{route('advertisings.single',['advertising'=>$item->id])}}">
                                                            @if(!$item->client_info->Hasmedia('images'))
                                                                <img src="{{asset('img/no-img.gif')}}" alt="{{$item->title}}"  width="50" />
                                                            @else
                                                                <img src="{{$item->client_info->getFirstMediaUrl('images')}}"  alt="{{$item->title}}" width="50"  />
                                                            @endif
                                                        </a>
                                                        <br>
                                                        <a href="{{route('companies.single',['company'=>$item->client_info->company->id])}}">
                                                            <span>{{$item->client_info->company->name}}</span>
                                                        </a>
                                                    </div>
                                                    <div class="jp_job_post_right_cont">
                                                        <h4><a href="{{route('advertisings.single',['advertising'=>$item->id])}}">{{$item->title}}</a></h4>

                                                        <ul>
                                                            <li><i class="flaticon-cash"></i>{{showSalary($item->salary)}}</li>
                                                            <li><i class="flaticon-location-pointer"></i> {{TranslateCountry($item->country)}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="jp_job_post_right_btn_wrapper jb_cover">
                                                        <ul>
                                                            @foreach(json_decode($item->kind,true) as $value)

                                                            <li><a href="#">{{\Modules\Advertising\Entities\TypeAdvertising::whereTitle($value)->first()->symbol}}</a></li>
                                                            @endforeach
                                                               @if(auth('client')->check())
                                                                    @if(!hasAction($item,auth('client')->user(),'advertising') && hasSeeker())

                                                                    @else
                                                                        <li>
                                                                            <a href="{{route('advertisings.single',['advertising'=>$item->id])}}" >بررسی</a>
                                                                        </li>
                                                                    @endif
                                                                        <li>
                                                                            <a href="{{route('client.dashboard')}}"> وارد سایت شوید</a>
                                                                        </li>
                                                                @endif

                                                        </ul>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    @endforeach

                                    @elseif(isset($query) && count($query) > 0)

                                    @foreach($query as $q)
                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                <div class="job_listing_left_fullwidth job_listing_grid_wrapper jb_cover">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="jp_job_post_side_img">
                                                                <a href="{{route('advertisings.single',['advertising'=>$q->id])}}">
                                                                    @if(!$q->client_info->Hasmedia('images'))
                                                                        <img src="{{asset('img/no-img.gif')}}"  alt="{{$q->title}}"  width="80" height="60"/>
                                                                    @else
                                                                        <img src="{{$q->client_info->getFirstMediaUrl('images')}}"  alt="{{$q->title}}" width="80"  />
                                                                    @endif
                                                                </a>
                                                                <br>
                                                                <a href="{{route('companies.single',['company'=>$q->client_info->company->id])}}">
                                                                    <span>{{$q->client_info->company->name}}</span>
                                                                </a>
                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><a href="{{route('advertisings.single',['advertising'=>$q->id])}}">{{$q->title}}</a></h4>

                                                                <ul>
                                                                    <li><i class="flaticon-cash"></i>{{showSalary($q->salary)}}</li>
                                                                    <li><i class="flaticon-location-pointer"></i>&nbsp; {{TranslateCountry($q->country)}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="jp_job_post_right_btn_wrapper jb_cover">
                                                                <ul>
                                                                    @foreach(json_decode($q->kind,true) as $value)
                                                                        <li><a href="#">{{\Modules\Advertising\Entities\TypeAdvertising::whereTitle($value)->first()->symbol}}</a></li>
                                                                    @endforeach

                                                                        @if(auth('client')->check())
                                                                            @if(!hasAction($item,auth('client')->user(),'advertising') && hasSeeker())

                                                                            @elseif(!auth('client')->check())
                                                                                <li>
                                                                                    <a href="{{route('advertisings.single',['advertising'=>$q->id])}}" >بررسی</a>
                                                                                </li>
                                                                            @endif
                                                                        @else
                                                                            <li>
                                                                                <a href="{{route('client.dashboard')}}"> وارد سایت شوید</a>
                                                                            </li>
                                                                        @endif
                                                                        <li>
                                                                            <a href="{{route('client.dashboard')}}" >برای ارسال رزومه وارد سابت شوید</a>
                                                                        </li>
                                                                </ul>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @else
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="job_listing_left_fullwidth jb_cover">

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="error_top_wrapper jb_cover text-center">
                                                            <img src="{{asset('template/images/error.png')}}" alt="img" class="img-reponsive" width="300">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                    @endif


                                </div>
                            </div>
                            <div id="list" class="tab-pane fade">
                                <div class="row">

                                    @if(!isset($query))
                                    @foreach($items as $item)

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="job_listing_left_fullwidth jb_cover">
                                                <div class="row">
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                                        <a class="jp_job_post_side_img">
                                                            <a href="{{route('advertisings.single',['advertising'=>$item->id])}}">
                                                                @if(!$item->client_info->Hasmedia('images'))
                                                                <img src="{{asset('img/no-img.gif')}}"  alt="{{$item->title}}"  width="80" height="60">
                                                            @else
                                                                <img src="{{$item->client_info->getFirstMediaUrl('images')}}" alt="{{$item->title}}"  width="60" >
                                                            @endif
                                                           </a>
                                                        </a>
                                                            <br>
                                                                <a href="{{route('companies.single',['company'=>$item->client_info->company->id])}}">
                                                                    <span>{{$item->client_info->company->name}}</span>
                                                                </a>
                                                        </div>
                                                        <div class="jp_job_post_right_cont">
                                                            <h4><a href="#">{{$item->title}}</a></h4>

                                                            <ul>
                                                                <li><i class="flaticon-cash"></i>{{showSalary($item->salary)}}</li>
                                                                <li><i class="flaticon-location-pointer"></i>&nbsp;
                                                                    {{TranslateCountry($item->country)}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                                        <div class="jp_job_post_right_btn_wrapper">
                                                            <ul>
                                                                <li><a href="#">نیمه وقت</a></li>
                                                                @if(auth('client')->check())
                                                                    @if(hasCv($item))

                                                                    @else
                                                                        <li>
                                                                        <a href="{{route('advertisings.single',['advertising'=>$item->id])}}" >بررسی</a>
                                                                        </li>
                                                                    @endif
                                                                @else
                                                                    <li>
                                                                        <a href="{{route('client.dashboard')}}"> وارد سایت شوید</a>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>


                                            </div>
                                        </div>

                                    @endforeach

                                    @elseif(isset($query) && count($query) > 0)


                                        @foreach($query as $q)

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="job_listing_left_fullwidth jb_cover">
                                                    <div class="row">
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                                            <div class="jp_job_post_side_img">
                                                                <a href="{{route('advertisings.single',['advertising'=>$q->id])}}">
                                                                @if(!$q->client_info->Hasmedia('images'))
                                                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$q->title}}" width="80" height="60">
                                                                @else
                                                                    <img src="{{$q->client_info->getFirstMediaUrl('images')}}" alt="{{$q->title}}"  width="60" >
                                                                @endif
                                                            </a>
                                                                <br>
                                                                    <a href="{{route('companies.single',['company'=>$q->client_info->company->id])}}">
                                                                        <span>{{$q->client_info->company->name}}</span>
                                                                    </a>

                                                            </div>
                                                            <div class="jp_job_post_right_cont">
                                                                <h4><a href="#">{{$q->title}}</a></h4>

                                                                <ul>
                                                                    <li><i class="flaticon-cash"></i>{{showSalary($q->salary)}}</li>
                                                                    <li><i class="flaticon-location-pointer"></i>
                                                                        {{TranslateCountry($q->country)}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                                            <div class="jp_job_post_right_btn_wrapper">
                                                                <ul>
                                                                    <li><a href="#">نیمه وقت</a></li>
                                                                    @if(auth('client')->check())
                                                                        @if(hasCv($q))

                                                                        @else
                                                                            <li>
                                                                            <a href="{{route('advertisings.single',['advertising'=>$q->id])}}" >بررسی</a>
                                                                            </li>
                                                                        @endif
                                                                    @else
                                                                        <li>
                                                                            <a href="{{route('client.dashboard')}}"> وارد سایت شوید</a>
                                                                        </li>
                                                                    @endif
                                                                </ul>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @else
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="job_listing_left_fullwidth jb_cover">

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="error_top_wrapper jb_cover text-center">
                                                            <img src="{{asset('template/images/error.png')}}" alt="img" class="img-reponsive" width="300">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endif


                                </div>
                            </div>
                            <div class="blog_pagination_section jb_cover">
                                @if(!isset($query))
                                {!! $items->links() !!}
                                @else
                                    {!! $query->links() !!}
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-12 d-block d-sm-block d-md-block d-lg-none d-xl-none">

                    <form action="{{route('filter.advert')}}" method="GET">

                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#order" role="button">
                            <h6>مرتب سازی </h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{(isset($query) && $order=='new') || (isset($query) && $order=='view') ? "in show " :"" }}" id="order">

                            <p class="job_field">
                                <label for="f1">
                                    <input type="radio" {{isset($query) && $order=='new' ? "checked" : ""}} value="new" id="f1" name="order">

                                    جدیدترین
                                </label>
                            </p>
                            <p class="job_field">
                                <label for="f2">
                                    <input type="radio" {{isset($query) && $order=='view' ? "checked" : ""}} value="view" id="f2" name="order">
                                    پربازدید ترین
                                </label>
                            </p>


                        </div>
                    </div>

                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#category" role="button">
                            <h6>دسته بندی </h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_category) || $array_category==null ? "" :"in show" }}"  id="category">
                            @foreach($all_category_advertisings as $category)
                                <p >
                                    <input type="checkbox" {{isset($query) && in_array($category->id,$array_category) ? "checked " :"" }} value="{{$category->id}}" id="c1{{$category->id}}" name="cb">
                                    <label for="c1{{$category->id}}"  {{isset($query) && in_array($category->id,$array_category) ? "checked " :"" }}>{{$category->symbol}}</label>
                                </p>
                            @endforeach

                        </div>

                    </div>
                        <div class="job_filter_category_sidebar jb_cover">
                            <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#company" role="button">
                                <h6>شرکت  </h6>
                            </div>

                            <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_company) || $array_company==null ? "" :"in show" }}" id="company">
                                @foreach($companies as $company)
                                    <p >
                                        <input type="checkbox" {{isset($query) && in_array($company->id,$array_company) ? "checked " :"" }} value="{{$company->token}}" id="com{{$company->id}}" name="company[]">
                                        <label for="com{{$company->id}}">{{$company->company->name}}</label>
                                    </p>
                                @endforeach

                            </div>

                        </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#skill" role="button">
                            <h6>مهارت</h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($array_skill) || $array_skill==null ? "" :"in show" }}" id="skill">
                            @foreach($skills as $skill)
                                <p >
                                    <input type="checkbox" {{isset($query) && in_array($skill->title,$array_skill) ? "checked " :"" }} value="{{$skill->title}}" id="s{{$skill->id}}" name="skill[]">
                                    <label for="s{{$skill->id}}">{{$skill->symbol}}</label>
                                </p>
                            @endforeach

                        </div>
                    </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover" data-toggle="collapse" href="#salary" role="button">
                            <h6>حقوق</h6>
                        </div>

                        <div class="category_jobbox jb_cover collapse multi-collapse {{empty($item_salary) || $item_salary==null ? "" :"in show" }}" id="salary">
                            @foreach($salaries as $salary)
                                <p >
                                    <input type="checkbox" {{isset($query) && $salary->title==$item_salary ? "checked" : ""}} value="{{$salary->title}}" id="sa{{$salary->id}}" name="salary">
                                    <label for="sa{{$salary->id}}">{{$salary->symbol}}</label>
                                </p>
                            @endforeach


                        </div>
                    </div>

                        <div class="job_filter_category_sidebar jb_cover">
                            <input type="submit" class="btn btn-block btn-pink" style="color: #fdfdfd"  value="اعمال فیلتر">
                        </div>

                    </form>

                    <div class="jp_add_resume_wrapper jb_cover">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">

                            <a class="ad" target="_blank" href="{{getAd('75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r')->href}}" data-param="75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r">
                                @if(getAd('75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r')->status==1 && expireTime(getAd('75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r')->start_at,getAd('75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r')->end_at))

                                    <img src="{{getAd('75eTYRtn1i6NC8pCl0eRyTD2TgACUdpW55r')->getFirstMediaUrl('images')}}" alt="{{$setting->name}}"  width="80" height="60"/>
                                @else
                                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" width="60"  />
                                @endif
                            </a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--job listing filter  wrapper end-->


@endsection

@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('script')

    <script>
            $("a.ad").bind("click", function () {
                $code=$(this).data('param');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/count/ad/'+$code,
                    data: { field1:$code} ,
                    success: function (response) {
                    },
                    error: function (response) {

                    },
                });
            });


    </script>


@endsection

