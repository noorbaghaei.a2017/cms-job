@extends('template.app')

@section('content')


    <!-- navi wrapper End -->

    <!--job single wrapper start-->
    <div class="job_single_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    @include('core::layout.alert-danger')
                    @include('core::layout.alert-success')
                </div>
            </div>
            <div class="row">

                <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover">
                            <h6>توضیحات</h6>
                        </div>
                        <div class="job_overview_header jb_cover">
                            <div class="jb_job_overview_img">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}"  alt="post_img" width="100">
                                @else
                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="post_img" width="100" >
                                @endif
                                <h4>{{$item->title}} ({{showExperience($item->work_experience)}}  .)</h4>
{{--                                <a href="{{route('companies.single',['company'=>$item->info_company->id])}}">--}}
{{--                                    <p>{{$item->info_company->name}}</p>--}}
{{--                                </a>--}}

                                <ul class="job_single_lists">
{{--                                    <li>--}}
{{--                                        <div class="job_adds_right">--}}
{{--                                            <a href="#!"><i class="far fa-heart"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
                                    <li>
                                        <div class="header_btn search_btn part_time_btn jb_cover">
                                    @foreach(json_decode($item->kind,true) as $value)
                                                <a href="#">{{\Modules\Advertising\Entities\TypeAdvertising::whereTitle($value)->first()->symbol}}</a>
                                    @endforeach

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="far fa-calendar"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>
                                        <li>{{Morilog\Jalali\Jalalian::forge($item->created_at)->ago()}}</li>

                                    </ul>
                                </div>
                            </div>
                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>
                                        <li>{{TranslateCountry($item->country)}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="fa fa-info-circle"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>

                                        <li>{{$item->title}}</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="far fa-money-bill-alt"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>

                                        <li>{{showSalary($item->salary)}} ریال</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="fa fa-th-large"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>

                                        <li>{{showCategory($item->category)->symbol}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                <div class="jp_listing_list_icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="jp_listing_list_icon_cont_wrapper">
                                    <ul>

                                        <li>{{showExperience($item->work_experience)}} </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="header_btn search_btn news_btn overview_btn  jb_cover">


                                @if(auth('client')->check())
                                        @if(!hasAction($item,auth('client')->user(),'advertising') && hasSeeker())
                                        <a href="#" data-toggle="modal" data-target="#myModal41">درخواست کنید !</a>
                                        <div class="modal fade apply_job_popup" id="myModal41" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <form action="{{route('send.cv',['token'=>$item->token])}}" method="POST" enctype="multipart/form-data" >
                                                                @csrf
                                                                <div class="apply_job jb_cover">
                                                                    <h1>درخواست برای  کار : {{$item->title}}</h1>
                                                                    <div class="search_alert_box jb_cover">


                                                                        <p class="word_file"> حداکثر فایل مجاز برای ارسال (1 مگابایت)</p>
                                                                        <div class="resume_optional jb_cover">
                                                                            <div class="text-center">
                                                                                <input type="file" name="document">
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                    <div class="header_btn search_btn applt_pop_btn jb_cover">

                                                                        <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd">درخواست کنید</button>

                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif(!auth('client')->check())
                                            <a href="{{route('client.dashboard')}}" >برای ارسال رزومه وارد سابت شوید</a>
                                            @endif
                                @elseif(!auth('client')->check())
                                    <a href="{{route('client.dashboard')}}" >برای ارسال رزومه وارد سابت شوید</a>
                                @endif



                            </div>

                        </div>
                        <div class="jb_keyword_key_wrapper jb_cover">
                            <ul>
                                <li><i class="fa fa-tags"></i> کلمات کلیدی مورد علاقه :</li>
                                @foreach($item->tags as $tag)
                                <li><a href="#">#{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="jb_listing_single_overview jb_cover">
                        <div class="jp_job_des jb_cover">
                            <h2 class="job_description_heading">شرح شغل</h2>

   {!! $item->text !!}
                           <ul>
                                <li><i class="fas fa-globe-asia"></i>&nbsp;&nbsp; <a href="#">www.example.com</a></li>

                            </ul>
                        </div>
                        <div class="jp_job_res jb_cover">
                            <h2 class="job_description_heading">مسئولیت ها</h2>
                            <ul>
                                @foreach(json_decode($item->job_position,true) as $value)
                                <li><i class="fa fa-caret-right"></i>{{\Modules\Advertising\Entities\Position::whereTitle($value)->first()->symbol}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="jp_job_res jb_cover minimum_cover">
                            <h2 class="job_description_heading">مدارک تحصیلی مورد نیاز</h2>

                            <ul>
                                @foreach(json_decode($item->education,true) as $value)
                                <li><i class="fa fa-caret-right"></i>{{\Modules\Advertising\Entities\Education::whereTitle($value)->first()->symbol}}</li>

                                @endforeach
                            </ul>
                        </div>
                        <div class="jp_job_res jb_cover">
                            <h2 class="job_description_heading">مهارت ها</h2>
                            <ul>
                                @foreach(json_decode($item->skill,true) as $value)
                                    <li><i class="fa fa-caret-right"></i>{{\Modules\Advertising\Entities\Skill::whereTitle($value)->first()->symbol}}</li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="jp_job_res jp_listing_left_wrapper jb_cover">
                            <div class="jp_listing_left_bottom_sidebar_social_wrapper">
                                <ul>
                                    <li>اشتراک :</li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="related_job_wrapper jb_cover">
                        <h1 class="related_job">آگهی های مشابه</h1>
                        <div class="related_product_job jb_cover">

                            <div >
                                <div class="item">
                                    @foreach($similar_advertisings as $similar)
                                    <div class="job_listing_left_fullwidth jb_cover">
                                        <div class="row">
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                                <div class="jp_job_post_side_img">
                                                    @if(!$similar->Hasmedia('images'))
                                                        <img src="{{asset('img/no-img.gif')}}"  alt="post_img" width="60">
                                                    @else
                                                        <img src="{{$similar->getFirstMediaUrl('images')}}" alt="post_img" width="60" >
                                                    @endif

                                                    <br> <span>گوگل</span>
                                                </div>
                                                <div class="jp_job_post_right_cont">
                                                    <h4><a href="{{route('advertisings.single',['advertising'=>$similar->id])}}">{{$similar->title}}</a></h4>

                                                    <ul>
                                                        <li><i class="flaticon-cash"></i>&nbsp; {{showSalary($similar->salary)}}</li>
                                                        <li><i class="flaticon-location-pointer"></i>&nbsp;{{TranslateCountry($similar->country)}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                                <div class="jp_job_post_right_btn_wrapper">
                                                    <ul>
                                                        <li>
                                                            <div class="job_adds_right">
                                                                <a href="#!"><i class="far fa-heart"></i></a>
                                                            </div>
                                                        </li>
                                                        <li><a href="#">نیمه وقت</a></li>
                                                        @if(auth('client')->check())
                                                        @if(!hasAction($item,auth('client')->user(),'advertising') && hasSeeker())
                                                            <a href="#" data-toggle="modal" data-target="#myModal41">درخواست کنید !</a>
                                                            <div class="modal fade apply_job_popup" id="myModal41" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                                                <form action="{{route('send.cv',['token'=>$item->token])}}" method="POST" enctype="multipart/form-data" >
                                                                                    @csrf
                                                                                    <div class="apply_job jb_cover">
                                                                                        <h1>درخواست برای  کار : {{$item->title}}</h1>
                                                                                        <div class="search_alert_box jb_cover">


                                                                                            <p class="word_file"> حداکثر فایل مجاز برای ارسال (1 مگابایت)</p>
                                                                                            <div class="resume_optional jb_cover">
                                                                                                <div class="text-center">
                                                                                                    <input type="file" name="document">
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="header_btn search_btn applt_pop_btn jb_cover">

                                                                                            <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd">درخواست کنید</button>

                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif(!auth('client')->check())
                                                            <a href="{{route('client.dashboard')}}" >برای ارسال رزومه وارد سابت شوید</a>
                                                        @endif
                                                        @else
                                                            <a href="{{route('client.dashboard')}}" >برای ارسال رزومه وارد سابت شوید</a>
                                                        @endif
                                                    </ul>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--job single wrapper end-->


@endsection

