
@extends('template.app')

@section('content')

    <!--companies wrapper start -->
    <div class="companies_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="filter-area jb_cover">


                    </div>
                </div>

                @foreach($items as $item)

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="company_main_wrapper">
                            <div class="company_img_wrapper">
                                <a href="{{route('companies.single',['company'=>$item->id])}}">
                                    @if(!$item->Hasmedia('images'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="{{$item->company->name}}" style="width: 80px !important;">
                                    @else
                                        <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->company->name}}" style="width: 80px !important;">
                                    @endif


                                </a>
{{--                                <div class="btc_team_social_wrapper">--}}
{{--                                    <h1>(تهران)</h1>--}}
{{--                                </div>--}}
                            </div>
                            <div class="opening_job">
                                <h1><a href="{{route('companies.single',['company'=>$item->id])}}">4 شغل بار</a></h1></div>
                            <div class="company_img_cont_wrapper">
                                <a href="{{route('companies.single',['company'=>$item->id])}}">
                                    <h4>{{$item->company->name}}</h4>
                                </a>


                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>
    <!-- companies wrapper end -->
@endsection
