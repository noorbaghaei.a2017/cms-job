

<!DOCTYPE html>
<!--
Template Name: JB desks
Version: 1.0.0
Author: Webstrot

-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zxx">
<!--[endif]-->

<head>
    <meta charset="utf-8" />
    {!! SEO::generate() !!}

    @yield('seo')
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="MobileOptimized" content="320" />
    <!--Template style -->
    <meta name="google-site-verification" content="{{env('GOOGLE_VERIFY')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/fonts.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/flaticon.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/font-awesome.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/dropify.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/nice-select.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/reset.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/responsive.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/css/select2.min.css')}}" />

    @yield('head')

    <!--favicon-->
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
</head>

<body>
<!-- preloader Start -->
<div class="jb_preloader">
    <div class="spinner_wrap">
        <div class="spinner"></div>
    </div>
</div>
<div class="cursor"></div>
<!-- Top Scroll Start --><a href="javascript:" id="return-to-top"><i class="fas fa-angle-double-up"></i></a>
<!-- Top Scroll End -->
<!-- cp navi wrapper Start -->
<nav class="cd-dropdown  d-block d-sm-block d-md-block d-lg-none d-xl-none">
    <h2><a href="{{route('front.website')}}"> <span>
                @if(!$setting->Hasmedia('logo'))
                    <img src="{{asset('img/no-img.gif')}}" alt="img" width="80" height="50">
                @else
                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="img" width="50" >
                @endif

            </span></a></h2>
    <a href="#0" class="cd-close">بستن</a>
    <ul class="cd-dropdown-content">
            @foreach($top_menus as $menu)
                <li><a href="{{$menu->href}}">{{$menu->symbol}}</a></li>
            @endforeach
    </ul>
    <!-- .cd-dropdown-content -->
</nav>
<div class="cp_navi_main_wrapper jb_cover">
    <div class="container-fluid">
        <div class="cp_logo_wrapper">
            <a href="{{route('front.website')}}">
                @if(!$setting->Hasmedia('logo'))
                    <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="60">
                @else
                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="50" >

                @endif

            </a>
        </div>
        <!-- mobile menu area start -->
        <header class="mobail_menu d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cd-dropdown-wrapper">
                            <a class="house_toggle" href="#0">
                                <i class="fas fa-bars" style="color: #fdfdfd"></i>
                            </a>
                            <!-- .cd-dropdown -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- .cd-dropdown-wrapper -->
        </header>
      @include('template.sections.status')

       @include('template.sections.menu')
    </div>
</div>

<!-- navi wrapper End -->
