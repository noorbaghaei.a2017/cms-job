@foreach($new_articles->take(2) as $article)
<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="our_blog_content jb_cover">
        <div class="jb_cover">
            <a href="{{route('articles.single',['article'=>$article->slug])}}">
            @if(!$article->Hasmedia('images'))
                <img src="{{asset('img/no-img.gif')}}" class="img-responsive" alt="{{$article->title}}" title="{{$article->title}}" style="width: 100%">
            @else
                <img src="{{$article->getFirstMediaUrl('images', 'medium')}}" class="img-responsive" alt="{{$article->title}}" title="{{$article->title}}" style="width: 100%">
            @endif
            </a>

        </div>
        <div class="blog_content jb_cover">
            <p>{{Morilog\Jalali\Jalalian::forge($article->create_at)->format('%A, %d %B %y')}}</p>
            <h4> <a href="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</a></h4>
        </div>
    </div>
</div>
@endforeach

