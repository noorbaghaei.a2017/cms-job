<div class="menu_btn_box header_btn jb_cover">
    <ul>

        @if(!auth('client')->check())
            <li>
                <a href="{{route('client.dashboard')}}"> ورود</a>
            </li>
        @else

            <li>
                <a class="client" href="{{route('client.dashboard')}}">
                    @if(!auth('client')->user()->Hasmedia('images'))
                        <img src="{{asset('img/no-img.gif')}}" alt="img" width="70">
                    @else
                        <img src="{{auth('client')->user()->getFirstMediaUrl('images')}}" alt="img" width="70">
                    @endif
                </a>
            </li>
        @endif
    </ul>
</div>
