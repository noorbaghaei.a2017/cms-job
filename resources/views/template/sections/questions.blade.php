@if(\Modules\Core\Helper\CoreHelper::hasQuestion())
<div class="col-lg-12 col-mjb_banner_wrapper jb_coverd-12 col-sm-12">
    <div id="accordion" role="tablist">
        <h1>پرسش و پاسخ متداول</h1>
        @foreach($questions as $question)
            <div class="card">

                <div class="card_pagee" role="tab" id="heading1">
                    <h5 class="h5-md">
                        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
                            {{$question->title}}

                        </a>
                    </h5>
                </div>

                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="heading1" data-parent="#accordion" style="">
                    <div class="card-body">

                        <div class="card_cntnt">
<p>
    {{$question->answer}}
</p>
                        </div>
                    </div>
                </div>

            </div>
        @endforeach

    </div>
</div>
@endif
