@if(\Modules\Core\Helper\CoreHelper::hasMember())
<!-- team wrapper start-->
<div class="team_wrapper jb_cover">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                <div class="jb_heading_wraper">

                    <h3>کارمندان ما</h3>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="team_slider_wrapper jb_cover">
                    <div class="owl-carousel owl-theme">
                        @foreach($members as $member)
                        <div class="item">

                            <div class="team_slider_content jb_cover">
                                <div class="team_slider_img_box jb_cover">
                                    <img src="{{asset('template/images/team.png')}}" alt="img" class="img-responsive">
                                </div>
                                <div class="team_slider_content_btm jb_cover">
                                    <p>(مشاور ارشد)</p>
                                    <h2><a href="#">{{$member->full_name}}</a></h2>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- team wrapper end-->
@endif
