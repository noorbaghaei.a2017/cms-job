<div class="jb_navigation_wrapper">
    <div class="mainmenu d-xl-block d-lg-block d-md-none d-sm-none d-none">
        <ul class="main_nav_ul">
            @foreach($top_menus as $menu)
            <li><a href="{{$menu->href}}" class="gc_main_navigation">{{$menu->symbol}}</a></li>
            @endforeach
        </ul>
    </div>
    <!-- mainmenu end -->

</div>
