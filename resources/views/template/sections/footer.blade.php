<!-- footer Wrapper Start -->
<div class="footer jb_cover">

    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="footerNav jb_cover">
                    <a href="{{route('front.website')}}">
                        @if(!$setting->Hasmedia('logo'))
                            <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="80">
                        @else
                            <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="60">

                        @endif
                    </a>
                    <ul class="footer_first_contact">
                        <li><i class="flaticon-location-pointer"></i>
                           <p>
                               {{$setting->address}}
                           </p>
                        </li>

                        <li>
                            <i class="flaticon-envelope"></i><a href="mailto::{{$setting->email}}">{{$setting->email}} </a>
                            <br>
                        </li>

                    </ul>

                    <ul class="icon_list_news jb_cover">
                        <li>
                            <a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fab fa-telegram"></i></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fab fa-whatsapp"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="footerNav jb_cover footer_border_displ">
                    <h5>ویژگی ها</h5>
                    <ul class="nav-widget">
                        @foreach($properties as $property)
                        <li><a href="#"><i class="fa fa-square"></i>
                                {{$property->title}}
                            </a></li>
                        @endforeach


                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="footerNav float_left footer_border_displ">
                    <h5>خدمات</h5>
                    <ul class="nav-widget">

                        @foreach($services as $service)
                        <li><a href="#"><i class="fa fa-square"></i>{{$service->title}}</a></li>
                        @endforeach


                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="footerNav float_left footer_border_displ">
                    <h5>برنامه ها</h5>
                    <ul class="nav-widget">
                        <li>
                            <a href="#"><img src="{{asset('template/images/android.png')}}" alt="img" width="20">andorid
                            </a>
                        </li>

                        <li>
                            <a href="#"><img src="{{asset('template/images/ios.png')}}" alt="img" width="20">ios
                            </a>
                        </li>



                    </ul>
                </div>
            </div>
            <div class="copyright_left"><i class="fa fa-copyright"></i> تمامی این حقوقو برای کارنیک محفوط می باشد
            </div>

            <div class="clearfix"></div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
    <div class="waveWrapper waveAnimation">
        <div class="waveWrapperInner bgTop gradient-color">
            <div class="wave waveTop"></div>
        </div>
        <div class="waveWrapperInner bgMiddle">
            <div class="wave waveMiddle"></div>
        </div>
        <div class="waveWrapperInner bgBottom">
            <div class="wave waveBottom"></div>
        </div>
    </div>

</div>

<!-- footer Wrapper End -->

<!--custom js files-->
<script src="{{asset('template/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<script src="{{asset('template/js/modernizr.js')}}"></script>
<script src="{{asset('template/js/jquery.menu-aim.js')}}"></script>
<script src="{{asset('template/js/plugin.js')}}"></script>
<script src="{{asset('template/js/owl.carousel.js')}}"></script>
<script src="{{asset('template/js/jquery-ui.js')}}"></script>
<script src="{{asset('template/js/jquery.countTo.js')}}"></script>
<script src="{{asset('template/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('template/js/dropify.min.js')}}"></script>
<script src="{{asset('template/js/jquery.inview.min.js')}}"></script>
<script src="{{asset('template/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('template/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('template/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('template/js/custom.js')}}"></script>
<script src="{{asset('template/js/select2.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();

        $('.CodeMirror').css('display','none')

    });
</script>


@yield('script')

<!-- custom js-->
</body>

</html>
