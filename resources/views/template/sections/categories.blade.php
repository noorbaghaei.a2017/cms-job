
<!-- job category wrapper start-->
<div class="jb_category_wrapper jb_cover" style="padding-top: 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                <div class="jb_heading_wraper">

                    <h3>دسته بندی ها</h3>

                </div>
            </div>
            @foreach($category_advertisings as $category)

                <div class="col-lg-3 col-md-6 col-sm-12" style="margin-bottom: 15px">
                    <form method="GET" action="{{route('filter.advert')}}">
                        @csrf
                    <div class="jb_browse_category jb_cover">
                        <input hidden class="pixel-radio" name="category[]" checked value="{{$category->id}}" type="checkbox" >
                        <button type="submit" style="cursor: pointer;border: none;background: transparent">
                        <div class="hover-block"></div>

                            <i class="flaticon-code"></i>

                                <h6>{{$category->symbol}}</h6>

                            <p>{{count($category->advertisings)}}</p>
                        </button>
                    </div>
                    </form>
                </div>

            @endforeach
            @if(count($all_category_advertisings) > 4)
            <div class="header_btn search_btn load_btn jb_cover">

                <a href="{{route('category-advertising')}}">مشاغل بیشتر</a>

            </div>
            @endif
        </div>
    </div>
</div>
<!-- job category wrapper end-->
