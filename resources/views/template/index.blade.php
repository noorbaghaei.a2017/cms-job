﻿@extends('template.app')

@section('content')

    <div style="padding-top: 10px">
        <div class="container">
            <div class="row">


                <div class="col-lg-6 col-md-12 col-12 col-sm-12" >

                    <div class="grow_next_img jb_cover">
                       <a href="{{route('client.register.employer')}}">
                           <img src="{{asset('template/images/employer.png')}}" style="width: 100%">
                       </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 col-sm-12" >
                    <div class="grow_next_img jb_cover">
                        <a href="{{route('client.register')}}">
                            <img src="{{asset('template/images/seeker.png')}}" style="width: 100%">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@include('template.sections.categories')

<!-- grow next Wrapper Start -->
    <div class="grow_next_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                    <div class="jb_heading_wraper left_jb_jeading">

                        <h3>رشد کسب و کار سطح بعدی</h3>

                        <p># 1 بیشترین اعتماد شرکت بازار دیجیتال</p>
                    </div>
                    <div class="grow_next_text jb_cover">
                        <p>همه مشاوران چه نیاز دارند؟ به طور خلاصه، اعتماد. این با ارائه حرفه ای و توانایی برقراری ارتباط با مشتریان و مشتریان بالقوه حاصل می شود. آیا شما یک حسابدار هستید.
                            <br>
                        <div class="header_btn search_btn jb_cover">

                            <a href="{{route('contact-us')}}">تماس با ما</a>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                    <div class="grow_next_img jb_cover">
                        <img src="{{asset('template/images/content.png')}}" class="img-responsive" alt="img" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- grow next Wrapper end -->
    <!-- counter wrapper start-->
    <div class="counter_wrapper jb_cover">
        <div class="counter_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="counter_mockup_design jb_cover">
                        <div class="animation-circle-inverse"><i></i><i></i><i></i></div>
                        <img src="{{asset('template/images/mockup2.png')}}" class="img-responsive" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="counter_right_wrapper jb_cover">
                        <div class="counter_width">
                            <div class="counter_cntnt_box">

                                <div class="count-description"><span class="timer">{{$employerscount}}</span>
                                    <p class="con2">کارفرما </p>
                                </div>
                            </div>
                        </div>
                        <div class="counter_width">
                            <div class="counter_cntnt_box">

                                <div class="count-description"> <span class="timer">{{$seekerscount}}</span>
                                    <p class="con2">کارجو</p>
                                </div>
                            </div>
                        </div>
                        <div class="counter_width">
                            <div class="counter_cntnt_box">

                                <div class="count-description"> <span class="timer">{{$advertisingscount}}</span>
                                    <p class="con2">آگهی</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- counter wrapper end-->

    <!-- company Wrapper start -->
    <div class="top_company_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="top_hiring_cpmpany_heading jb_cover">
                        <div class="jb_heading_wraper left_jb_jeading">

                            <h3> شرکت های برتر</h3>

                        </div>
                        <p>همه مشاوران چه نیاز دارند؟ به طور خلاصه، اعتماد. این با ارائه حرفه ای و توانایی برقراری ارتباط به دست می آید. با مشتریان موجود و بالقوه واضح است. مشاوران نیاز دارند؟ به طور خلاصه، اعتماد. این است که آیا شما آیا شما یک حسابدار است.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="top_company_slider_wrapper jb_cover">
                        <div >
                            <div class="item">
                                <div class="row">
                                    @foreach($popular_companies as $popular)
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="company_main_wrapper">
                                            <div class="company_img_wrapper">
                                                <a href="{{route('companies.single',['company'=>$popular->id])}}">
                                                @if(!$popular->Hasmedia('images'))
                                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$popular->company->name}}" style="width: 60px !important;">
                                                @else
                                                    <img src="{{$popular->getFirstMediaUrl('images')}}" alt="{{$popular->company->name}}" title="{{$popular->company->name}}" style="width: 60px !important;">
                                                @endif
                                                </a>
                                            </div>
                                            <div class="opening_job">
                                                <form method="GET" action="{{route('filter.advert')}}">
                                                    @csrf
                                                    <div class="jb_browse_category jb_cover">
                                                        <input hidden class="pixel-radio" name="company[]" checked value="{{$popular->company->id}}" type="checkbox" >
                                                        <button class="btn btn-pink" type="submit" style="cursor: pointer;border: none;background: transparent">
                                                            فرصت های شغلی
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

                                    @endforeach
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <!-- top company Wrapper End -->--}}
   @include('template.sections.plans')

    <!-- download app wrapper start-->
    <div class="download_wrapper jb_cover">
        <div class="counter_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="download_mockup_design jb_cover">
                        <div class="animation-circle-inverse2"><i></i><i></i><i></i></div>
                        <img src="{{asset('template/images/mockup5.png')}}" class="img-responsive" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="download_app_store jb_cover">
                        <h1>دانلود</h1>
                        <h2>برنامه برنامه   {{$setting->name}}!</h2>
                        <p>30 ثانیه طول می کشد تا دانلود کنید. برنامه موبایل شما برای شغل
                            <br> سریع، ساده و دلپذیر.</p>
                        <div class="app_btn jb_cover">
                            <a href="#" class="ss_playstore"><span><i class="flaticon-android-logo"></i></span> پلی استور</a>
                            <a href="#" class="ss_appstore"><span><i class="flaticon-apple"></i></span> اپ استور</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- download app wrapper end-->
    <!-- blog wrapper start-->
    <div class="blog_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                    <div class="jb_heading_wraper">

                        <h3>{{$setting->name}} همیشه در کنار شما</h3>

                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="blog_newsleeter jb_cover">
                        <h1>درخواست تماس</h1>

                        <p>در کوتاه ترین زمان مشاوران ما با شما تماس میگیرند</p>

                        <div class="contect_form3 blog_letter">

                            <input type="text" name="subject" placeholder="عنوان" autocomplete="off">
                        </div>
                        <div class="contect_form3 blog_letter">

                            <input type="text" name="name" placeholder="نام " autocomplete="off">
                        </div>
                        <div class="contect_form3 blog_letter">

                            <input type="email" name="email" placeholder="ایمیل " autocomplete="off">
                        </div>
                        <div class="contect_form3 blog_letter">

                            <input type="number" name="mobile" placeholder="موبایل " autocomplete="off">
                        </div>
                        <div class="header_btn search_btn submit_btn jb_cover">

                            <a href="#">ثبت </a>

                        </div>
                    </div>

                </div>
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">
                        @include('template.sections.articles')

                    @include('template.sections.questions')
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="job_newsletter_wrapper jb_cover">
                        <div class="jb_newslwtteter_left">
                            <h2> به دنبال یک شغل</h2>
                            <p>لیست همه مشاغل ها </p>
                        </div>
                        <div class="jb_newslwtteter_button">
                            <div class="header_btn search_btn news_btn jb_cover">

                                <a href="{{route('advertisings')}}">جستجو</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog wrapper end-->



@endsection





