@extends('template.app')

@section('content')

    <!--job listing filter  wrapper start-->
    <div class="blog_single_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">

                    <div class="jp_first_blog_post_main_wrapper jb_cover">
                        <div class="jp_first_blog_post_img">
                            @if(!$item->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" class="img-responsive" alt="{{$item->title}}" title="{{$item->title}}" style="width: 100%">
                            @else
                                <img src="{{$item->getFirstMediaUrl('images')}}" class="img-responsive" alt="{{$item->title}}" title="{{$item->title}}" style="width: 100%">
                            @endif
                        </div>
                        <div class="jp_first_blog_post_cont_wrapper">
                            <p><span>{{Morilog\Jalali\Jalalian::forge($item->create_at)->format('%A, %d %B %y')}}</span></p>
                            <h3>
                                <a href="#">

                            {{$item->title}}
                                </a>
                            </h3>
                           {!! $item->text !!}
                        </div>
                        <div class="jp_first_blog_bottom_cont_wrapper jb_cover">
                            <div class="jp_blog_bottom_left_cont">
                                <ul>
                                    <li>
                                       @if(!$item->user_info->hasMedia('images'))

                                            <img src="{{asset('img/no-img.gif')}}" alt="{{$item->user_info->full_name }}" title="{{$item->user_info->full_name }}" width="30"/>
                                        @else
                                            <img src="{{$item->user_info->getFirstMediaUrl('images')}}" alt="{{$item->user_info->full_name }}" title="{{$item->user_info->full_name }}" width="30"/>

                                        @endif

                                        {{$item->user_info->full_name}}
                                    </li>
                                </ul>
                            </div>
                            <div class="jp_blog_bottom_right_cont">
                                <ul>
                                    <li></li>
                                    <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="#"><i class="fab fa-telegram"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-12">

                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover">
                            <h6>دسته بندی </h6>
                        </div>

                        <div class="category_jobbox jb_cover">
                            <ul class="blog_category_link jb_cover">
                                @foreach($categories as $category)
                                <li><i class="fa fa-caret-right"></i> <a href="#">{{$category->symbol}} </a></li>
                                @endforeach
                              </ul>
{{--                            <div class="seeMore"><a href="#">مشاهده تمام دسته بندی ها</a></div>--}}
                        </div>
                    </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover">
                            <h6>مشاوره شغلی</h6>
                        </div>

{{--                        <div class="category_jobbox jb_cover">--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/blog_ct1.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">جستجوگرهای کار - 2019</a></h4>--}}
{{--                                    <p><i class="far fa-calendar"></i>&nbsp;20 دی 1398</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/blog_ct2.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">جستجوگرهای کار - 2019</a></h4>--}}
{{--                                    <p><i class="far fa-calendar"></i> &nbsp;20 دی 1398</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/blog_ct3.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">جستجوگرهای کار - 2019</a></h4>--}}
{{--                                    <p><i class="far fa-calendar"></i> &nbsp;20 دی 1398</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="seeMore"><a href="#">مشاهده همه </a></div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover">
                            <h6>مرکز توجه کار</h6>
                        </div>

{{--                        <div class="category_jobbox jb_cover">--}}
{{--                            <div class="jp_spotlight_slider_wrapper">--}}
{{--                                <div class="owl-carousel owl-theme">--}}
{{--                                    <div class="item">--}}
{{--                                        <div class="jp_spotlight_slider_img_Wrapper">--}}
{{--                                            <img src="images/spt1.jpg" alt="spotlight_img" />--}}
{{--                                        </div>--}}
{{--                                        <div class="jp_spotlight_slider_cont_Wrapper">--}}
{{--                                            <h4>توسعه دهنده (1 تا 2 سال تجربه.)</h4>--}}

{{--                                            <p><a href="#">شرکت فن آوری وب استروت</a></p>--}}
{{--                                            <ul>--}}
{{--                                                <li><i class="far fa-money-bill-alt"></i>&nbsp; 12هزار - 15هزار تومان</li>--}}
{{--                                                <li><i class="fas fa-map-marker-alt"></i>&nbsp; کالیفورنیا</li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="header_btn search_btn news_btn overview_btn  jb_cover">--}}

{{--                                            <a href="#">درخواست کنید !</a>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    <div class="item">--}}
{{--                                        <div class="jp_spotlight_slider_img_Wrapper">--}}
{{--                                            <img src="images/spt1.jpg" alt="spotlight_img" />--}}
{{--                                        </div>--}}
{{--                                        <div class="jp_spotlight_slider_cont_Wrapper">--}}
{{--                                            <h4>توسعه دهنده (1 تا 2 سال تجربه.)</h4>--}}
{{--                                            <p><a href="#">شرکت فن آوری وب استروت</a></p>--}}
{{--                                            <ul>--}}
{{--                                                <li><i class="far fa-money-bill-alt"></i>&nbsp; 12هزار - 15هزار تومان</li>--}}
{{--                                                <li><i class="fas fa-map-marker-alt"></i>&nbsp; کالیفورنیا</li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="header_btn search_btn news_btn overview_btn  jb_cover">--}}

{{--                                            <a href="#">درخواست کنید !</a>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    <div class="item">--}}
{{--                                        <div class="jp_spotlight_slider_img_Wrapper">--}}
{{--                                            <img src="images/spt1.jpg" alt="spotlight_img" />--}}
{{--                                        </div>--}}
{{--                                        <div class="jp_spotlight_slider_cont_Wrapper">--}}
{{--                                            <h4>توسعه دهنده (1 تا 2 سال تجربه.)</h4>--}}
{{--                                            <p><a href="#">شرکت فن آوری وب استروت</a></p>--}}
{{--                                            <ul>--}}
{{--                                                <li><i class="far fa-money-bill-alt"></i>&nbsp; 12هزار - 15هزار تومان</li>--}}
{{--                                                <li><i class="fas fa-map-marker-alt"></i>&nbsp; کالیفورنیا</li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="header_btn search_btn news_btn overview_btn  jb_cover">--}}

{{--                                            <a href="#">درخواست کنید !</a>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="job_filter_category_sidebar jb_cover">
                        <div class="job_filter_sidebar_heading jb_cover">
                            <h6>رزومه های اخیر</h6>
                        </div>

{{--                        <div class="category_jobbox jb_cover">--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/rs1.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">آرش <br>خادملو</a></h4>--}}
{{--                                    <p><i class="fa fa-folder-open"></i> &nbsp; خواننده</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/rs2.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">آرش <br> خادملو</a></h4>--}}
{{--                                    <p><i class="fa fa-folder-open"></i> &nbsp; طراح</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="jp_rightside_career_content_wrapper jb_cover">--}}
{{--                                <div class="jp_rightside_career_img">--}}
{{--                                    <img src="images/rs3.jpg" alt="career_img" />--}}
{{--                                </div>--}}
{{--                                <div class="jp_rightside_career_img_cont">--}}
{{--                                    <h4><a href="#">آرش <br> خادملو</a></h4>--}}
{{--                                    <p><i class="fa fa-folder-open"></i>&nbsp; توسعه دهنده</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="seeMore"><a href="#">مشاهده همه رزومه ها</a></div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="jp_add_resume_wrapper jb_cover">
                        <div class="jp_add_resume_img_overlay"></div>
                        <div class="jp_add_resume_cont">
                            @if(!$setting->Hasmedia('logo'))
                                <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}"  title="{{$setting->name}}"/>

                            @else
                                <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}"  title="{{$setting->name}}" />
                            @endif

                            <h4>دریافت بهترین کارهای مرتبط در ایمیل شما. افزودن رزومه شرکت!</h4>
                            <div class="width_50">
                                <input type="file" id="input-file-now-custom-233" class="dropify" data-height="90" /><span class="post_photo">افزودن رزومه</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog single wrapper end-->

@endsection
