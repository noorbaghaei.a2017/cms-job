@extends('template.sections.user.app')

@section('content')

    <!--employee dashboard wrapper start-->

    <div class="candidate_dashboard_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-lg-12">
                            @include('core::layout.alert-danger')
                            @include('core::layout.alert-success')
                        </div>
                    </div>
                    <div class="emp_dashboard_sidebar jb_cover">

                        @if(!$client->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" class="img-responsive" alt="img">
                        @else
                            <img src="{{$client->getFirstMediaUrl('images')}}" class="img-responsive" alt="img">
                        @endif
                        <div class="emp_web_profile candidate_web_profile jb_cover">

                            <h4>{{$client->full_name}}</h4>
                            <p>{{$client->username}}</p>
                            @if(hasCode($client))
                                <p>{{isset(\Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code) ?  \Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code: ""}}</p>
                            @endif
                           @include('template.auth.sections.advert-plan')
                                <div class="skills jb_cover">
                                <div class="skill-item jb_cover">
                                    <h6>پروفایل<span>{{calPercentProfile($client)}}%</span></h6>
                                    <div class="skills-progress"><span data-value="{{calPercentProfile($client)}}%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="emp_follow_link jb_cover">
                            <ul class="feedlist">
                                <li><a href="{{route('client.dashboard')}}" ><i class="fas fa-tachometer-alt"></i> داشبورد </a></li>
                                <li>
                                    <a href="{{route('client.edit')}}"> <i class="fas fa-edit"></i>ویرایش پروفایل
                                    </a>
                                </li>
                                @if(hasEmployer())

                                    <li><a href="{{route('client.cvs')}}"><i class="fas fa-file"></i>رزومه </a></li>

                                    <li><a href="{{route('client.plans')}}"><i class="fas fa-file"></i>پلن ها </a></li>

                                    <li><a href="{{route('client.seekers')}}"><i class="fas fa-users"></i>کارجویان  </a></li>


                                @elseif(hasSeeker())

                                    <li><a href="{{route('client.advert')}}"><i class="fas fa-newspaper"></i>درخواست های همکاری </a></li>


                                    <li><a href="{{route('client.favorite')}}"><i class="fas fa-heart"></i>علاقه مندی ها </a></li>

                                @endif

                                <li><a href="{{route('client.wallet')}}" class="link_active"><i class="fas fa-wallet"></i>کیف پول </a></li>



                            </ul>
                            <ul class="feedlist logout_link jb_cover">
                                <li><a href="{{route('client.logout.panel')}}"><i class="fas fa-power-off"></i> خروج  </a></li>

                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">


                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="job_filter_category_sidebar jb_cover">
                                <div class="job_filter_sidebar_heading jb_cover">
                                    <h1>  کیف پول </h1>
                                </div>
                                <div class="job_overview_header jb_cover">

                                    <div class="row">
                                        <div class="col-lg-4 text-center">
                                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                                <div class="">
                                                    <i class="fa fa-coins"></i>
                                                </div>
                                                <div class="">
                                                    <ul>
                                                        <li>{{$client->wallet->coin}} سکه</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 text-center">
                                            <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                                <div class="">
                                                    <i class="fas fa-circle"></i>
                                                </div>
                                                <div class="">
                                                    <ul>
                                                        <li>{{$client->wallet->score}} امتیاز</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--employee dashboard wrapper end-->


@endsection

