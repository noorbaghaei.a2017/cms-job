@extends('template.app')
@section('content')

    <!-- navi wrapper End -->

    <!-- login wrapper start -->
    <div class="login_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="login_top_box jb_cover">

                        <div class="login_banner_wrapper">

                        </div>
                        <form action="{{ route('employer.verify.mobile') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">

                            @csrf


                            <div class="login_form_wrapper">
                                <div style="margin-bottom: 30px">
                                    @if(!$setting->Hasmedia('logo'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="60">
                                    @else
                                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="80" >
                                    @endif
                                </div>
                                <h2>ثبت نام کارفرما</h2>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" value="{{old('first_name')}}" name="first_name" placeholder="نام " autocomplete="off">
                                    <i class="fas fa-user"></i>
                                </div>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" value="{{old('last_name')}}" name="last_name" placeholder="نام خانوادگی" autocomplete="off">
                                    <i class="fas fa-users"></i>
                                </div>
                                @error('company')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" value="{{old('company')}}" name="company" placeholder="نام شرکت" autocomplete="off">
                                    <i class="fas fa-users"></i>
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" value="{{old('email')}}" name="email" placeholder="آدرس ایمیل" autocomplete="off">
                                    <i class="fas fa-envelope"></i>

                                </div>
                                @error('mobile')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" value="{{old('mobile')}}" name="mobile" placeholder="موبایل " autocomplete="off">
                                    <i class="fas fa-mobile"></i>

                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">

                                    <input type="password" class="form-control require" name="password" placeholder="رمزعبور " autocomplete="off">
                                    <i class="fas fa-lock"></i>
                                </div>
                                @error('code')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <div class="form-group icon_form comments_form">

                                    <input type="text" class="form-control require"  name="code" placeholder="کد معرف " autocomplete="off">
                                    <i class="fas fa-code"></i>
                                </div>

                                <div class="header_btn search_btn login_btn jb_cover">

                                    <input class="btn btn-success btn-block" type="submit" value="ثبت نام">
                                </div>
                                <div class="dont_have_account jb_cover">
                                    <p>حساب کاربری دارید؟ <a href="{{route('client.dashboard')}}">ورود</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login wrapper end -->

@endsection




