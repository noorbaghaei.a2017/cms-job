@extends('template.sections.user.app')

@section('content')

    <!--employee dashboard wrapper start-->

    <div class="candidate_dashboard_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-lg-12">
                            @include('core::layout.alert-danger')
                            @include('core::layout.alert-success')
                        </div>
                    </div>
                    <div class="emp_dashboard_sidebar jb_cover">

                        @if(!$client->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" class="img-responsive" alt="{{$client->full_name}}" title="{{$client->full_name}}">
                        @else
                            <img src="{{$client->getFirstMediaUrl('images')}}" class="img-responsive" alt="{{$client->full_name}}" title="{{$client->full_name}}">
                        @endif
                        <div class="emp_web_profile candidate_web_profile jb_cover">

                            <h4>{{$client->full_name}}</h4>
                            <p>{{$client->username}}</p>
                            @if(hasCode($client))
                            <p>{{isset(\Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code) ?  \Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code: ""}}</p>
                            @endif

                            @if(hasEmployer() &&  !expirePlan())
                                <a href="#" data-toggle="modal" class="new-advert" data-target="#advert">ثبت آگهی</a>
                                <div class="modal fade apply_job_popup" id="advert" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <form action="{{route('add.advert.employer')}}" method="POST" enctype="multipart/form-data" >
                                                        @csrf
                                                        <div class="apply_job jb_cover">
                                                            @if(!expirePlan())
                                                                <h1 style="background: #fc3267;color:#fdfdfd"> {{accessCountAdvert()}}  آگهی دیگر شما میتوانید ثبت کنید </h1>
                                                            @endif
                                                            <div class="search_alert_box jb_cover">


                                                                <div class="form-group row text-right">

                                                                    <p class="word_file"> حداکثر تصویر مجاز برای ارسال عکس (1 مگابایت)</p>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <input type="file" name="image">
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="title" class="form-control-label text-right">{{__('cms.text')}}</label>
                                                                        <input type="text" name="title" class="form-control" id="title" required autocomplete="off">
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="text" class="form-control-label text-right">{{__('cms.text')}}</label>
                                                                        <input type="text" name="text" class="form-control" id="text" required autocomplete="off">
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="excerpt" class="form-control-label text-right">{{__('cms.excerpt')}}</label>
                                                                        <input type="text" name="excerpt" class="form-control" id="excerpt" required autocomplete="off">
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="gender" class="form-control-label">{{__('cms.gender')}}  </label>
                                                                        <select class="form-control" id="gender" name="gender" required>

                                                                            <option  value="1" selected>{{__('cms.male')}}</option>
                                                                            <option  value="0">{{__('cms.female')}}</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="number_employees" class="form-control-label">{{__('cms.number_employees')}} </label>
                                                                        <input type="text" name="number_employees" value="{{old('number_employees')}}" class="form-control" id="number_employees" required autocomplete="off">

                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="currency"  class="form-control-label">  {{__('cms.currency')}}  </label>
                                                                        <select dir="rtl" class="form-control" id="currency" name="currency" required>
                                                                            @foreach($currencies as $currency)
                                                                                <option value="{{$currency->token}}" >{{$currency->symbol}}</option>
                                                                            @endforeach


                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
                                                                        <select dir="rtl" class="form-control" id="category" name="category" required>
                                                                            @foreach($all_category_advertisings as $category)
                                                                                <option value="{{$category->token}}" {{$loop->first ? "selected" : ""}}>{{$category->symbol}}</option>
                                                                            @endforeach

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="positions" class="form-control-label">{{__('cms.job_position')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="positions" name="positions[]" required multiple>
                                                                            @foreach($positions as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="kinds" class="form-control-label">{{__('cms.kind_advertising')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="kinds" name="kinds[]" required multiple>
                                                                            @foreach($kinds as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="experience" class="form-control-label">{{__('cms.work_experience')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="experience" name="experience" required >
                                                                            @foreach($experiences as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="guild" class="form-control-label">{{__('cms.guild')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="guild" name="guild" required>
                                                                            @foreach($guilds as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="salary" class="form-control-label">{{__('cms.salary')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="salary" name="salary" required >
                                                                            @foreach($salaries as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="skills" class="form-control-label">{{__('cms.skill_advertising')}} </label>
                                                                        <select dir="rtl" class="form-control"  id="skills" name="skills[]" required multiple>
                                                                            @foreach($skills as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="educations" class="form-control-label">{{__('cms.educations')}} </label>

                                                                        <select dir="rtl" class="form-control"  id="educations" name="educations[]" required multiple>
                                                                            @foreach($educations as $key=>$value)

                                                                                <option value="{{$value->title}}">{{$value->symbol}}</option>
                                                                            @endforeach
                                                                        </select>

                                                                    </div>



                                                                </div>

                                                            </div>
                                                            <div class="header_btn search_btn applt_pop_btn jb_cover">

                                                                <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd;background: #fc3267">ثبت </button>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <a href="#" data-toggle="modal" class="new-advert" data-target="#plan">خرید اشتراک</a>
                                <div class="modal fade apply_job_popup" id="plan" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <form action="#" method="POST" enctype="multipart/form-data" >
                                                        @csrf
                                                        <div class="apply_job jb_cover">

                                                            <h1 style="background: #fc3267;color:#fdfdfd"> اشتراک مورد نظر را انتخاب کنید</h1>

                                                            <div class="search_alert_box jb_cover">


                                                                <div class="form-group row text-right">

                                                                    <div class="col-lg-12" style="margin-bottom: 10px">
                                                                        <span class="text-danger">*</span>
                                                                        <label for="plan"  class="form-control-label">  {{__('cms.plan')}}  </label>
                                                                        <select dir="rtl" class="form-control" id="plan" name="plan" required>
                                                                            @foreach($plans_advertisings as $plan)
                                                                                <option value="{{$plan->token}}" >{{$plan->symbol}}</option>
                                                                            @endforeach


                                                                        </select>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="header_btn search_btn applt_pop_btn jb_cover">

                                                                <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd;background: #fc3267">خرید اشتراک </button>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                                <div class="skills jb_cover">
                                <div class="skill-item jb_cover">
                                    <h6>پروفایل<span>{{calPercentProfile($client)}}%</span></h6>
                                    <div class="skills-progress"><span data-value="{{calPercentProfile($client)}}%"></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="emp_follow_link jb_cover">
                            <ul class="feedlist">
                                <li><a href="{{route('client.dashboard')}}" class="link_active"><i class="fas fa-tachometer-alt"></i> داشبورد </a></li>
                                <li>
                                    <a href="{{route('client.edit')}}"> <i class="fas fa-edit"></i>ویرایش پروفایل
                                    </a>
                                </li>
                                @if(hasEmployer())

                                    <li><a href="{{route('client.cvs')}}"><i class="fas fa-file"></i>رزومه </a></li>



                                    <li><a href="{{route('client.plans')}}"><i class="fas fa-file"></i>پلن ها </a></li>

                                    <li><a href="{{route('client.seekers')}}"><i class="fas fa-users"></i>کارجویان  </a></li>


                                @elseif(hasSeeker())

                                    <li><a href="{{route('client.advert')}}"><i class="fas fa-newspaper"></i>درخواست های همکاری </a></li>


                                    <li><a href="{{route('client.favorite')}}"><i class="fas fa-heart"></i>علاقه مندی ها </a></li>

                                @endif

                                <li><a href="{{route('client.wallet')}}" ><i class="fas fa-wallet"></i>کیف پول </a></li>


                            </ul>
                            <ul class="feedlist logout_link jb_cover">
                                <li><a href="{{route('client.logout.panel')}}"><i class="fas fa-power-off"></i> خروج  </a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="modal fade delete_popup" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                        <div class="delett_cntn jb_cover">
                                            <h1><i class="fas fa-trash-alt"></i> حذف حساب</h1>
                                            <p>شما مطمئن هستید! شما میخواهید پروفایل خود را حذف کنید.
                                                <br> نمی تواند برگردانده شود!</p>

                                            <div class="delete_jb_form">

                                                <input type="password" name="password" placeholder="رمز عبور را وارد کنید">
                                            </div>
                                            <div class="header_btn search_btn applt_pop_btn">

                                                <a href="#">ذخیره به روزرسانی</a>

                                            </div>
                                            <div class="cancel_wrapper">
                                                <a href="#" class="" data-dismiss="modal">لغو</a>
                                            </div>
                                            <div class="login_remember_box jb_cover">
                                                <label class="control control--checkbox">شما شرایط و ضوابط <a href="#">و حریم خصوصی </a> ما را <a href="#">میپذیرید</a>
                                                    <input type="checkbox">
                                                    <span class="control__indicator"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="job_filter_category_sidebar jb_cover">
                                <div class="job_filter_sidebar_heading jb_cover">
                                    <h1> اطلاعات پایه</h1>
                                </div>
                                <div class="job_overview_header jb_cover">

                                    <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                        <div class="jp_listing_list_icon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>{{$client->about}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                        <div class="jp_listing_list_icon">
                                            <i class="fas fa-map-marker-alt"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li>{{isNot($client->country) ? " " : TranslateCountry($client->country)}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                        <div class="jp_listing_list_icon">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li><a href="tel:{{$client->mobile}}">{{$client->mobile}}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jp_listing_overview_list_main_wrapper jb_cover">
                                        <div class="jp_listing_list_icon">
                                            <i class="fas fa-envelope"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li><a href="mailto:{{$client->email}}" style="text-transform: none;">{{$client->email}}</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    @if(hasInfo($client,'client'))
                                    <div class="jp_listing_overview_list_main_wrapper dcv jb_cover">
                                        <div class="jp_listing_list_icon">
                                            <i class="fas fa-globe-asia"></i>
                                        </div>
                                        <div class="jp_listing_list_icon_cont_wrapper">
                                            <ul>
                                                <li><a target="_blank" href="{{$client->info->website}}">{{$client->info->website}}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--employee dashboard wrapper end-->


@endsection

