@if(hasEmployer() &&  !expirePlan())
    <a href="#" data-toggle="modal" class="new-advert" data-target="#advert">ثبت آگهی</a>
    <div class="modal fade apply_job_popup" id="advert" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <form action="{{route('add.advert.employer')}}" method="POST" enctype="multipart/form-data" >
                            @csrf
                            <div class="apply_job jb_cover">
                                @if(!expirePlan())
                                    <h1 style="background: #fc3267;color:#fdfdfd"> {{accessCountAdvert()}}  آگهی دیگر شما میتوانید ثبت کنید </h1>
                                @endif
                                <div class="search_alert_box jb_cover">


                                    <div class="form-group row text-right">

                                        <p class="word_file"> حداکثر تصویر مجاز برای ارسال عکس (1 مگابایت)</p>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <input type="file" name="image">
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="title" class="form-control-label text-right">{{__('cms.text')}}</label>
                                            <input type="text" name="title" class="form-control" id="title" required autocomplete="off">
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="text" class="form-control-label text-right">{{__('cms.text')}}</label>
                                            <input type="text" name="text" class="form-control" id="text" required autocomplete="off">
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="excerpt" class="form-control-label text-right">{{__('cms.excerpt')}}</label>
                                            <input type="text" name="excerpt" class="form-control" id="excerpt" required autocomplete="off">
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="gender" class="form-control-label">{{__('cms.gender')}}  </label>
                                            <select class="form-control" id="gender" name="gender" required>

                                                <option  value="1" selected>{{__('cms.male')}}</option>
                                                <option  value="0">{{__('cms.female')}}</option>

                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="number_employees" class="form-control-label">{{__('cms.number_employees')}} </label>
                                            <input type="text" name="number_employees" value="{{old('number_employees')}}" class="form-control" id="number_employees" required autocomplete="off">

                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="currency"  class="form-control-label">  {{__('cms.currency')}}  </label>
                                            <select dir="rtl" class="form-control" id="currency" name="currency" required>
                                                @foreach($currencies as $currency)
                                                    <option value="{{$currency->token}}" >{{$currency->symbol}}</option>
                                                @endforeach


                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
                                            <select dir="rtl" class="form-control" id="category" name="category" required>
                                                @foreach($all_category_advertisings as $category)
                                                    <option value="{{$category->token}}" {{$loop->first ? "selected" : ""}}>{{$category->symbol}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="positions" class="form-control-label">{{__('cms.job_position')}} </label>
                                            <select dir="rtl" class="form-control"  id="positions" name="positions[]" required multiple>
                                                @foreach($positions as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="kinds" class="form-control-label">{{__('cms.kind_advertising')}} </label>
                                            <select dir="rtl" class="form-control"  id="kinds" name="kinds[]" required multiple>
                                                @foreach($kinds as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="experience" class="form-control-label">{{__('cms.work_experience')}} </label>
                                            <select dir="rtl" class="form-control"  id="experience" name="experience" required >
                                                @foreach($experiences as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="guild" class="form-control-label">{{__('cms.guild')}} </label>
                                            <select dir="rtl" class="form-control"  id="guild" name="guild" required>
                                                @foreach($guilds as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="salary" class="form-control-label">{{__('cms.salary')}} </label>
                                            <select dir="rtl" class="form-control"  id="salary" name="salary" required >
                                                @foreach($salaries as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="skills" class="form-control-label">{{__('cms.skill_advertising')}} </label>
                                            <select dir="rtl" class="form-control"  id="skills" name="skills[]" required multiple>
                                                @foreach($skills as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="educations" class="form-control-label">{{__('cms.educations')}} </label>

                                            <select dir="rtl" class="form-control"  id="educations" name="educations[]" required multiple>
                                                @foreach($educations as $key=>$value)

                                                    <option value="{{$value->title}}">{{$value->symbol}}</option>
                                                @endforeach
                                            </select>

                                        </div>



                                    </div>

                                </div>
                                <div class="header_btn search_btn applt_pop_btn jb_cover">

                                    <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd;background: #fc3267">ثبت </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <a href="#" data-toggle="modal" class="new-advert" data-target="#plan">خرید اشتراک</a>
    <div class="modal fade apply_job_popup" id="plan" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <form action="#" method="POST" enctype="multipart/form-data" >
                            @csrf
                            <div class="apply_job jb_cover">

                                <h1 style="background: #fc3267;color:#fdfdfd"> اشتراک مورد نظر را انتخاب کنید</h1>

                                <div class="search_alert_box jb_cover">


                                    <div class="form-group row text-right">

                                        <div class="col-lg-12" style="margin-bottom: 10px">
                                            <span class="text-danger">*</span>
                                            <label for="plan"  class="form-control-label">  {{__('cms.plan')}}  </label>
                                            <select dir="rtl" class="form-control" id="plan" name="plan" required>
                                                @foreach($all_plans_advertisings as $plan)
                                                    <option value="{{$plan->token}}">{{$plan->title}} - {{number_format($plan->price->amount)}}</option>
                                                @endforeach


                                            </select>
                                        </div>

                                    </div>

                                </div>
                                <div class="header_btn search_btn applt_pop_btn jb_cover">

                                    <button class="btn btn-pink btn-block" type="submit" style="color: #fdfdfd;background: #fc3267">خرید اشتراک </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
