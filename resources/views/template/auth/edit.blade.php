@extends('template.sections.user.app')

@section('content')

    <!--employee dashboard wrapper start-->
    <div class="candidate_dashboard_wrapper jb_cover">
        <div class="container">
<form action="{{route('seeker.update',['seeker'=>$client->token])}}" method="POST">
@method('PATCH')
    @csrf
            <div class="row">

                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="emp_dashboard_sidebar jb_cover">

                        @if(!$client->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" class="img-responsive" alt="img">
                        @else
                            <img src="{{$client->getFirstMediaUrl('images')}}" class="img-responsive" alt="img">
                        @endif
                        <div class="emp_web_profile candidate_web_profile jb_cover">

                            <h4>{{$client->full_name}}</h4>
                            <p>{{$client->username}}</p>
                            @if(hasCode($client))
                                <p>{{isset(\Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code) ?  \Modules\Core\Entities\Code::where('codeable_id',$client->id)->where('codeable_type',\Modules\Client\Entities\Client::class)->first()->code: ""}}</p>
                            @endif
                            <div class="skills jb_cover">
                                <div class="skill-item jb_cover">
                                    <h6>پروفایل<span>{{calPercentProfile($client)}}%</span></h6>
                                    <div class="skills-progress"><span data-value="{{calPercentProfile($client)}}%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="emp_follow_link jb_cover">
                            <ul class="feedlist">
                                <li><a href="{{route('client.dashboard')}}" ><i class="fas fa-tachometer-alt"></i> داشبورد </a></li>
                                <li>
                                    <a href="{{route('client.edit')}}" class="link_active"> <i class="fas fa-edit"></i>ویرایش پروفایل
                                    </a>
                                </li>
                                @if(hasEmployer())

                                    <li><a href="{{route('client.cvs')}}"><i class="fas fa-file"></i>رزومه </a></li>

                                    <li><a href="{{route('client.advertisings')}}"><i class="fas fa-newspaper"></i>آگهی ها </a></li>


                                    <li><a href="{{route('client.plans')}}"><i class="fas fa-file"></i>پلن ها </a></li>

                                    <li><a href="{{route('client.seekers')}}"><i class="fas fa-users"></i>کارجویان  </a></li>


                                @elseif(hasSeeker())

                                    <li><a href="{{route('client.advert')}}"><i class="fas fa-newspaper"></i>درخواست های همکاری </a></li>

                                    <li><a href="{{route('client.favorite')}}" ><i class="fas fa-heart"></i>علاقه مندی ها </a></li>

                                @endif

                                <li><a href="{{route('client.wallet')}}" ><i class="fas fa-wallet"></i>کیف پول </a></li>


                            </ul>
                            <ul class="feedlist logout_link jb_cover">
                                <li><a href="{{route('client.logout.panel')}}"><i class="fas fa-power-off"></i> خروج  </a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="modal fade delete_popup" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                        <div class="delett_cntn jb_cover">
                                            <h1><i class="fas fa-trash-alt"></i> حذف حساب</h1>
                                            <p>شما مطمئن هستید! شما میخواهید پروفایل خود را حذف کنید.
                                                <br> نمی تواند برگردانده شود!</p>

                                            <div class="delete_jb_form">

                                                <input type="password" name="password" placeholder="رمز عبور را وارد کنید">
                                            </div>
                                            <div class="header_btn search_btn applt_pop_btn">

                                                <a href="#">ذخیره به روزرسانی</a>

                                            </div>
                                            <div class="cancel_wrapper">
                                                <a href="#" class="" data-dismiss="modal">لغو</a>
                                            </div>
                                            <div class="login_remember_box jb_cover">
                                                <label class="control control--checkbox">شما شرایط و ضوابط <a href="#">و حریم خصوصی </a> ما را <a href="#">میپذیرید</a>
                                                    <input type="checkbox">
                                                    <span class="control__indicator"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">
                      <div class="col-lg-12">
                          @include('core::layout.alert-danger')
                          @include('core::layout.alert-success')
                      </div>
                    </div>
                    <div class="row text-center">


                        <input style="margin: 0 auto" class="btn btn-info btn-block" type="submit" value="بروز رسانی">

                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12" style="padding: 0">

                            <div class="browse_img_banner jb_cover">

                                <div class="row">
                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <span class="text-danger">*</span>
                                        <label for="firstname" class="form-control-label">{{__('cms.first-name')}}</label>
                                        <input type="text" name="first_name" value="{{$client->first_name}}" class="form-control" id="first_name"  >
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <span class="text-danger">*</span>
                                        <label for="lastname" class="form-control-label">{{__('cms.last-name')}}</label>
                                        <input type="text" name="last_name" value="{{$client->last_name}}" class="form-control" id="last_name"  >
                                    </div>

                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <span class="text-danger">*</span>
                                        <label for="identity_card" class="form-control-label">{{__('cms.identity_card')}}</label>
                                        <input type="text" name="identity_card" value="{{$client->identity_card}}" class="form-control" id="identity_card" >
                                    </div>


                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <label for="username" class="form-control-label">{{__('cms.username')}}</label>
                                        <input type="text" name="username" value="{{$client->username}}" class="form-control" id="username"  >
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <label for="email" class="form-control-label">{{__('cms.email')}}</label>
                                        <input type="text" name="email" value="{{$client->email}}" class="form-control" id="email"  >
                                    </div>

                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <label for="website" class="form-control-label">{{__('cms.website')}}</label>
                                        <input type="text" name="website" value="{{$client->info->website}}" class="form-control" id="website"  >
                                    </div>

                                    <div class="col-sm-6" style="margin-bottom: 10px">
                                        <label for="address" class="form-control-label">{{__('cms.address')}}</label>
                                        <input type="text" name="info_address" value="{{$client->info->address}}" class="form-control" id="address"  >
                                    </div>


                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="job_filter_category_sidebar jb_cover">
                                        <div class="job_filter_sidebar_heading jb_cover">
                                            <h1> شبکه های اجتماعی</h1>
                                        </div>
                                        <div class="job_overview_header jb_cover">
                                            <div class="row">
                                                <div class="col-sm-6" style="margin-bottom: 10px">
                                                    <label for="instagram" class="form-control-label">{{__('cms.instagram')}}</label>
                                                    <input type="text" name="instagram" value="{{$client->info->instagram}}" class="form-control" id="instagram"  >
                                                </div>
                                                <div class="col-sm-6" style="margin-bottom: 10px">
                                                    <label for="facebook" class="form-control-label">{{__('cms.facebook')}}</label>
                                                    <input type="text" name="facebook" value="{{$client->info->facebook}}" class="form-control" id="facebook"  >
                                                </div>
                                                <div class="col-sm-6" style="margin-bottom: 10px">
                                                    <label for="twitter" class="form-control-label">{{__('cms.twitter')}}</label>
                                                    <input type="text" name="twitter" value="{{$client->info->twitter}}" class="form-control" id="twitter"  >
                                                </div>
                                                <div class="col-sm-6" style="margin-bottom: 10px">
                                                    <label for="whatsapp" class="form-control-label">{{__('cms.whatsapp')}}</label>
                                                    <input type="text" name="whatsapp" value="{{$client->info->whatsapp}}" class="form-control" id="whatsapp"  >
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="job_filter_category_sidebar jb_cover">
                                        <div class="job_filter_sidebar_heading jb_cover">
                                            <h1> رمز عبور و امنیت</h1>
                                        </div>
                                        <div class="job_overview_header jb_cover">
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label for="current_password" class="form-control-label">{{__('cms.current_password')}} </label>
                                                    <input type="password" name="current_password" class="form-control" id="current_password">
                                                    <small class="help-block text-warning">{{__('cms.empty_password')}}</small>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="new_password" class="form-control-label">{{__('cms.new_password')}}</label>
                                                    <input type="password" name="new_password" class="form-control" id="new_password">
                                                    <small class="help-block text-warning">{{__('cms.empty_password')}}</small>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>


            </div>
</form>
        </div>

    </div>
    <!--employee dashboard wrapper end-->



@endsection

