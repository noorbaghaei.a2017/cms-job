@extends('template.app')
@section('content')

    <!-- navi wrapper End -->

    <!-- login wrapper start -->
    <div class="login_wrapper jb_cover">
        <div class="container">
            <div class="row">

                                <span style="opacity: 0" id="click" data-user="{{$client->token}}" >
                                </span>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="login_top_box jb_cover">
                        <div class="verify_banner_wrapper">

                        </div>



                            <div class="login_form_wrapper">
                                <div style="margin-bottom: 30px">
                                    @if(!$setting->Hasmedia('logo'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="60">
                                    @else
                                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="50" >
                                    @endif
                                </div>
                                <h2>تایید اطلاعات</h2>


                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong id="result"></strong>
                                    </span>

                                <div class="form-group icon_form comments_form">

                                    <input id="code" type="text" class="form-control require" name="code" placeholder="کد فعال سازی " autocomplete="off">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="header_btn search_btn login_btn jb_cover" id="container-ajaxStart">

                                    <button id="ajaxStart" onclick="getCode()" class="btn btn-success btn-block" >تایید و ورود</button>
                                </div>


                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login wrapper end -->


@endsection


@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('script')

    <script>
       function getCode() {
           $("#ajaxStart").attr("disabled", true);
            $user=$('#click').data('user');
            $code=$('#code').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/verify/'+$user+"/"+$code,
                data: { field1:$user,field2:$code} ,
                beforeSend: function(){
                    $("#ajaxStart").attr("disabled", true);
                },
                success: function (response) {
                    window.location.href = '/user/panel/dashboard'
                },
                error: function (response) {
                    $('#result').text('کد نامعتبر است');
                },
                complete:function(data){
                    $("#container-ajaxStart button").css("display", 'none');
                    $("#container-ajaxStart").append("<span>عملیات با موفقیت انحام شد در  حال اتصال به پنل کاربری شما لطفا منتطر باشید</span>");
                }
            });
           $("#ajaxStart").attr("disabled", false);
        }
    </script>

@endsection




