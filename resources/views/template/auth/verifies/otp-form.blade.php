@extends('template.app')
@section('content')

    <!-- navi wrapper End -->

    <!-- login wrapper start -->
    <div class="login_wrapper jb_cover">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="login_top_box jb_cover">
                        <div class="otp_banner_wrapper">

                        </div>


                        <form action="{{ route('client.otp.submit') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">

                            @csrf

                        <div class="login_form_wrapper">
                            <div style="margin-bottom: 30px">
                                @if(!$setting->Hasmedia('logo'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="60">
                                @else
                                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="50" >
                                @endif
                            </div>
                            <h2>ورود با تایید هویت بدون رمز عبور </h2>

                            @error('identify')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                            <div class="form-group icon_form comments_form">

                                <input id="identify" type="text" class="form-control require" name="identify" placeholder="شماره موبایل یا ایمیل " autocomplete="off">
                                <i class="fas fa-lock"></i>
                            </div>

                            <div class="header_btn search_btn login_btn jb_cover">

                                <button id="ajaxStart" onclick="getCode()" class="btn btn-success btn-block" >تایید و ورود</button>
                            </div>


                        </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login wrapper end -->


@endsection





