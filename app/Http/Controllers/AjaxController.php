<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Ad\Entities\Ad;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\State;
use Modules\Core\Entities\User;
use Modules\Product\Entities\CartList;
use Modules\Product\Entities\Product;

class AjaxController extends Controller
{
   public function verifyMobileSms($user,$code){
       $user=Client::whereToken($user)->first();
       if($user->code==$code){
           $user->update([
               'is_active'=>2,
               'expire'=>now()
           ]);

           auth('client')->loginUsingId($user->id);

           $msg="success";
           $data=[
               'result'=>1
           ];
           $status=200;
       }
       else{
           $msg="کد نامعتبر است";
           $data=[
               'result'=>null
           ];
           $status=400;
       }
       return response()->json(array('msg'=>$msg,'data'=>$data),$status);
   }
    public function loadCountries(){

        $item=Country::latest()->get()->toArray();

            $msg="success";
            $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadStates($country){

        $item=State::latest()->where('country_id',$country)->get()->toArray();
        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadCities($state){

        $item=City::latest()->where('state_id',$state)->get()->toArray();

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function countAd($ad){

        $item=Ad::where('token',$ad)->first();
        $item->analyzer->increment('view');

        $item=[
            'item'=>$item,
            'token'=>$ad
        ];

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function incProduct($token){

       $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
       $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
           'count'=>$data->count + 1,
            'price'=>$data->price + intval($product->price->amount)
       ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();

        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'increment',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'increment',
                'count'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }

    public function decProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
            'count'=>$data->count - 1,
            'price'=>$data->price - intval($product->price->amount)
        ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'decrement',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'decrement',
                'current'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function removeProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $deleted=CartList::whereCart($client->cart->id)->whereProduct($product->id)->delete();
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($deleted){
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
}
